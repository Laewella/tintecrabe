/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils } from '../services/core.mjs';

export let Strategies = {
	conditions: {},
	effects: {},
	completion: {},
	evaluation: {},
	registeredFeatures: {},
	registeredFeaturesNeighbours: {},
	/**
	 * @param {string} code
	 * @returns {ConditionStrategy}
	 */
	getCondition(code) {
		let strategy = this.conditions[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getCondition] Unknown strategy:', code);
		return this.conditions['null'];
	},
	/**
	 * @param {string} code
	 * @returns {ConditionStrategy}
	 */
	getEffect(code) {
		let strategy = this.effects[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEffect] Unknown strategy:', code);
		return this.effects['null'];
	},
	/**
	 * @param {string} code
	 * @returns {CompletionStrategy}
	 */
	getCompletion(code) {
		let strategy = this.completion[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getCompletion] Unknown strategy:', code);
		return this.completion['null'];
	},
	/**
	 * @param {string} code
	 * @returns {EvaluationStrategy}
	 */
	getEvaluation(code) {
		let strategy = this.evaluation[code];
		if (strategy) {
			return strategy;
		}
		console.warn('[Strategies.getEvaluation] Unknown strategy:', code);
		return this.evaluation['null'];
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Position[]} positions
	 */
	registerFeature(feature, box, positions) {
		positions.forEach((position) => {
			let x = position.x;
			let y = position.y;
			if (!this.registeredFeatures['r' + y]) {
				this.registeredFeatures['r' + y] = {};
			}
			if (!this.registeredFeatures['r' + y]['c' + x]) {
				this.registeredFeatures['r' + y]['c' + x] = [];
			}
			this.registeredFeatures['r' + y]['c' + x].push([feature, box]);
		});
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Array}
	 */
	getRegisteredFeatures(x, y) {
		if (Strategies.registeredFeatures['r' + y] && Strategies.registeredFeatures['r' + y]['c' + x]) {
			return Strategies.registeredFeatures['r' + y]['c' + x];
		}
		return [];
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Feature} neighbour
	 */
	registerFeatureNeighbour(feature, box, neighbour) {
		if (!Strategies.registeredFeaturesNeighbours['f' + neighbour.id]) {
			Strategies.registeredFeaturesNeighbours['f' + neighbour.id] = [];
		}
		Strategies.registeredFeaturesNeighbours['f' + neighbour.id].push([feature, box]);
	},
	/**
	 * @param {Feature} feature
	 * @returns {Array}
	 */
	getRegisteredNeighbours(feature) {
		return Strategies.registeredFeaturesNeighbours['f' + feature.id] || [];
	},
	/**
	 * @param {ConditionData[]} conditionsData
	 * @param {Structure} structure
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	checkCondition(conditionsData, structure, currentPlayer, newlyPlayedPawns, pawn) {
		let conditionsVerified = true;
		conditionsData.forEach((conditionData) => {
			let condition = Strategies.getCondition(conditionData.type);
			let result = condition.evaluate(structure, conditionData.configuration || {}, currentPlayer, newlyPlayedPawns, pawn);
			if ((result && conditionData.not) || (!result && !conditionData.not)) {
				conditionsVerified = false;
			}
		});
		return conditionsVerified;
	}
};

export class Structure {
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 */
	constructor(feature, box) {
		/** @type {Feature} */
		this.initialFeature = feature;
		/** @type {Box} */
		this.initialBox = box;
		/** @type {Feature[]} */
		this.features = [];
		/** @type {Box[]} */
		this.boxes = [];
		/** @type {Pawn[]} */
		this.pawns = [];
		/** @type {Feature[]} */
		this.featuresByPawnIndex = [];
		/** @type {Feature[][]} */
		this.featuresByBoxIndex = [];
		/** @type {Box[]} */
		this.boxesByPawnIndex = [];
		/** @type {Box[]} */
		this.boxesByFeatureIndex = [];
		/** @type {Feature[]} */
		this.openFeatures = [];
		/** @type {Feature[]} */
		this.occupiedFeatures = [];

		this.addFeature(feature, box);
	}

	/**
	 * @returns {number}
	 */
	get initialX() {
		return this.initialBox.x;
	}

	/**
	 * @returns {number}
	 */
	get initialY() {
		return this.initialBox.y;
	}

	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 */
	addFeature(feature, box) {
		if (ArrayUtils.contains(this.features, feature)) {
			return;
		}

		if (!ArrayUtils.contains(this.boxes, box)) {
			this.boxes.push(box);
			this.featuresByBoxIndex.push([]);
		}
		this.featuresByBoxIndex[this.boxes.indexOf(box)].push(feature);

		this.features.push(feature);
		this.boxesByFeatureIndex.push(box);
		if (feature.pawns.length) {
			this.occupiedFeatures.push(feature);
			feature.pawns.forEach((pawn) => {
				this.pawns.push(pawn);
				let pawnIndex = this.pawns.indexOf(pawn);
				this.featuresByPawnIndex[pawnIndex] = feature;
				this.boxesByPawnIndex[pawnIndex] = box;
			});
		}

		let open = false;
		for (let i = 0; i < 4; i++) {
			let neighbour = box.getNeighbour(i);
			for (let j = 0; j < 3; j++) {
				if (box.tile.getSideFeature(i, j) === feature) {
					if (!neighbour || neighbour.isEmpty) {
						open = true;
						continue;
					}
					this.addFeature(box.getRelatedFeature(i, j), neighbour);
				}
			}
		}

		if (open) {
			this.openFeatures.push(feature);
		}
	}

	/**
	 * @param {Feature} feature
	 * @returns {Box}
	 */
	getBoxByFeature(feature) {
		return this.boxesByFeatureIndex[this.features.indexOf(feature)];
	}

	/**
	 * @param {Pawn} pawn
	 * @returns {Box}
	 */
	getBoxByPawn(pawn) {
		return this.boxesByPawnIndex[this.pawns.indexOf(pawn)];
	}

	/**
	 * @returns {boolean}
	 */
	isClosed() {
		return this.openFeatures.length === 0;
	}

	/**
	 * @returns {boolean}
	 */
	isOccupied() {
		return this.occupiedFeatures.length > 0;
	}

	/**
	 * @returns {CompletionData[]}
	 */
	get completion() {
		return this.initialFeature.model.completion;
	}

	/**
	 * @returns {Array}
	 */
	get evaluationCompleted() {
		return this.initialFeature.model.evaluationCompleted;
	}

	/**
	 * @returns {Array}
	 */
	get evaluationEnd() {
		return this.initialFeature.model.evaluationEnd;
	}

	/**
	 * @returns {boolean}
	 */
	isCompleted() {
		let me = this;
		let result = true;
		this.completion.forEach(function(data) {
			let strategy = Strategies.getCompletion(data.type || 'null');
			if (!strategy.evaluate(me, data.configuration || {})) {
				result = false;
			}
		});
		return result;
	}

	register() {
		let me = this;
		this.completion.forEach(function(data) {
			let strategy = Strategies.getCompletion(data.type || 'null');
			strategy.register(me, data.configuration || {});
		});
	}

	/**
	 * @param {Feature} feature
	 * @param {GameContext} gameContext
	 */
	onContinued(feature, gameContext) {
		// Apply feature's effects.
		feature.model.continuation.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], this, gameContext.currentPlayer, gameContext.newlyPlayedPawns)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = this.getBoxByFeature(feature);
				let source = '(' + feature.name + ' [' + box.x + ', ' + box.y + '])';
				strategy.apply(this, feature, strategyData.configuration || {}, gameContext.currentPlayer, gameContext.newlyPlayedPawns, source);
			}
		});

		// Apply pawns effects.
		this.pawns.forEach((pawn) => {
			pawn.onContinued(this, feature, gameContext);
		});
	}

	/**
	 * @param {Feature} feature
	 * @param {Player} currentPlayer
	 * @param {Pawn[]} newlyPlayedPawns
	 */
	onCompleted(feature, currentPlayer, newlyPlayedPawns) {
		let me = this;
		let evaluator = new StructureEvaluator(this, feature, currentPlayer, newlyPlayedPawns);

		// Add points.
		evaluator.majors.forEach(function(player) {
			let points = evaluator.evaluateCompleted(player);
			player.addPoints(points, '(' + me.initialFeature.name + ' [' + me.initialBox.x + ', ' + me.initialBox.y + '])');
		});

		// Check neighbours.
		this.features.forEach(function(feature) {
			Strategies.getRegisteredNeighbours(feature).forEach(function(neighbour) {
				(new Structure(neighbour[0], neighbour[1])).checkCompletion();
			});
		});

		// Remove all pawn.
		this.removePawns();
	}

	onEndGame() {
		let me = this;
		let evaluator = new StructureEvaluator(this);

		// Add points.
		evaluator.majors.forEach(function(player) {
			let points = evaluator.evaluateEnd(player);
			player.addPoints(points, '(' + me.initialFeature.name + ' [' + me.initialBox.x + ', ' + me.initialBox.y + '])');
		});

		// Remove all pawn.
		this.removePawns();
	}

	checkCompletion() {
		if (this.isCompleted()) {
			this.onCompleted();
		}
	}

	/**
	 * @param {Player} player
	 * @param {string} type
	 * @returns {boolean}
	 */
	hasPlayerPawnOfType(player, type) {
		let has = false;
		this.pawns.forEach((pawn) => {
			if (pawn.owner === player && pawn.model.type === type) {
				has = true;
			}
		});
		return has;
	}

	/**
	 * @param {Player} player
	 * @param {string} code
	 * @returns {boolean}
	 */
	hasPlayerPawnOfCode(player, code) {
		let has = false;
		this.pawns.forEach((pawn) => {
			if (pawn.owner === player && pawn.model.code === code) {
				has = true;
			}
		});
		return has;
	}

	removePawns() {
		// Remove pawns.
		for (let i = 0; i < this.features.length; i++) {
			let feature = this.features[i];
			for (let j = 0; j < feature.pawns.length; j++) {
				feature.pawns[j].back();
			}
			feature.removePawns();
		}

		// Refresh display.
		for (let i = 0; i < this.boxes.length; i++) {
			this.boxes[i].refreshPawns();
		}
	}

	/**
	 * @param {string} status close|open|any
	 * @returns {boolean}
	 */
	checkStatus(status) {
		if (status === 'any') {
			return true;
		}
		else if (status === 'closed') {
			if (this.isClosed()) {
				return true;
			}
		}
		else if (status === 'open') {
			if (!this.isClosed()) {
				return true;
			}
		}
		else {
			console.warn('[Structure.checkStatus] Unknown featureStatus:', status);
		}
		return false;
	}

	/**
	 * @param {string[]} types
	 * @returns {Structure[]}
	 */
	getNeighbourStructuresByTypes(types) {
		let neighbourStructures = [];
		let alreadyTestedFeatures = [this.features];
		for (let i = 0; i < this.features.length; i++) {
			let feature = this.features[i];
			let box = this.getBoxByFeature(feature);
			let neighbours = feature.getNeighboursByTypes(types);
			for (let j = 0; j < neighbours.length; j++) {
				if (!ArrayUtils.contains(alreadyTestedFeatures, neighbours[j])) {
					let neighbourStructure = new Structure(neighbours[j], box);
					ArrayUtils.merge(alreadyTestedFeatures, neighbourStructure.features);
					neighbourStructures.push(neighbourStructure);
				}
			}
		}
		return neighbourStructures;
	}

	/**
	 * @param {Feature} feature
	 */
	getConnectionsCount(feature) {
		let count = 0;
		let box = this.getBoxByFeature(feature);
		box.tile.getSideDirectionsByFeature(feature).forEach((sideDirection) => {
			if (!box.getNeighbour(sideDirection).isEmpty) {
				count++;
			}
		});
		return count;
	}
}

export class StructureEvaluator {
	/**
	 * @param {Structure} structure
	 * @param {Feature=} feature
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 */
	constructor(structure, feature, currentPlayer, newlyPlayedPawns) {
		this.structure = structure;
		this.data = {};
		this.majors = [];
		this.currentPlayer = currentPlayer;
		this.newlyPlayedPawns = newlyPlayedPawns;

		let maxScore = 0;
		structure.pawns.forEach((pawn) => {
			let player = pawn.owner;
			let playerName = player.name;
			if (!(playerName in this.data)) {
				this.data[playerName] = {
					pawns: [],
					score: 0,
					major: false
				};
			}
			this.data[playerName].pawns.push(pawn);

			pawn.model.majority.forEach((majorityData) => {
				if (Strategies.checkCondition(majorityData.conditions || [], structure, currentPlayer, newlyPlayedPawns, pawn)) {
					let strategy = Strategies.getEvaluation(majorityData.type);
					this.data[playerName].score += strategy.evaluate(structure, majorityData.configuration, player, [pawn]);
					if (this.data[playerName].score) {
						if (this.data[playerName].score > maxScore) {
							this.majors = [player];
							maxScore = this.data[playerName].score;
						}
						else if (this.data[playerName].score === maxScore) {
							this.majors.push(player);
						}
					}
				}
			});
		});

		this.majors.forEach((player) => {
			this.data[player.name].major = true;
		});
	}

	/**
	 * @param player
	 * @returns {number}
	 */
	evaluateCompleted(player) {
		return this.evaluate(player, this.structure.evaluationCompleted);
	}

	/**
	 * @param player
	 * @returns {number}
	 */
	evaluateEnd(player) {
		return this.evaluate(player, this.structure.evaluationEnd);
	}

	/**
	 * @param {Player} player
	 * @param {EvaluationData[]} evaluationData
	 * @returns {number}
	 */
	evaluate(player, evaluationData) {
		let result = 0;
		evaluationData.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], this.structure, player, this.newlyPlayedPawns)) {
				let strategy = Strategies.getEvaluation(strategyData.type);
				result += Math.ceil(strategy.evaluate(this.structure, strategyData.configuration, player, this.data[player.name].pawns));
			}
		});
		return result;
	}
}

export class GameContext {
	/**
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Box[]=} newlyPlayedBoxes
	 */
	constructor(currentPlayer, newlyPlayedPawns, newlyPlayedBoxes) {
		this.currentPlayer = currentPlayer;
		this.newlyPlayedPawns = newlyPlayedPawns;
		this.newlyPlayedTiles = newlyPlayedBoxes;
	}
}