/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
let document = window.document;

export let Zoom = {
	level: 2,
	/**
	 * @param {number} level
	 */
	set: function(level) {
		Zoom.level = level;
	}
};

export class Box {
	/** @type {Tile|null} */
	tile;
	/** @type {boolean} */
	isEmpty;
	/** @type {string} */
	cssClasses;
	/** @type {Feature[]} */
	availableFeatures = [];
	/** @type {Feature[]} */
	occupiedFeatures = [];

	/**
	 * @param {number} x
	 * @param {number} y
	 */
	constructor(x, y) {
		this.x = x;
		this.y = y;
		this.tile = null;
		this.hasPreview = false;
		this.isPreviewValid = false;
		this.setEmpty();
		this.setAccessible(false);
	}

	/**
	 * @returns {Box[]}
	 */
	get neighbours() {
		let neighbours = [];
		let coordinates = [[this.x - 1, this.y], [this.x + 1, this.y], [this.x, this.y - 1], [this.x, this.y + 1]];
		for (let i = 0; i < coordinates.length; i++) {
			let neighbour = Boxes.get(coordinates[i][0], coordinates[i][1]);
			if (neighbour) {
				neighbours.push(neighbour);
			}
		}
		return neighbours;
	}

	/**
	 * @param {string|number} direction
	 * @returns {Box|null}
	 */
	getNeighbour(direction) {
		if (direction === 'top' || direction === 0) {
			return Boxes.get(this.x, this.y - 1);
		}
		else if (direction === 'right' || direction === 1) {
			return Boxes.get(this.x + 1, this.y);
		}
		else if (direction === 'bottom' || direction === 2) {
			return Boxes.get(this.x, this.y + 1);
		}
		else if (direction === 'left' || direction === 3) {
			return Boxes.get(this.x - 1, this.y);
		}
		console.error('[Box::getNeighbour] Invalid direction:', direction);
		return null;
	}

	/**
	 * @param {string|number} direction
	 * @param {number} index
	 * @returns {Feature|null}
	 */
	getRelatedFeature(direction, index) {
		let neighbour = this.getNeighbour(direction);
		if (!neighbour || neighbour.isEmpty) {
			return null;
		}
		return neighbour.tile.getOppositeFeature(direction, index);
	}

	/**
	 * @param {Tile} tile
	 */
	setTile(tile) {
		this.removePreview();
		this.tile = tile;
		this.setFull();
	}

	removeTile() {
		this.removePreview();
		this.tile = null;
		this.setEmpty();
	}

	setEmpty() {
		this.isEmpty = true;
		this.refreshCssClasses();
	}

	setFull() {
		this.isEmpty = false;
		this.refreshCssClasses();
	}

	refreshCssClasses() {
		let classes = ['box'];
		classes.push(this.isEmpty ? 'empty' : 'full');
		if (this.isAccessible) {
			classes.push('accessible');
		}
		if (this.hasPreview) {
			classes.push('preview');
			classes.push(this.isPreviewValid ? 'preview-valid' : 'preview-invalid');
		}
		this.cssClasses = classes.join(' ');
	}

	/**
	 * @param {boolean} value
	 */
	setAccessible(value) {
		if (value) {
			this.isAccessible = true;
			this.refreshCssClasses();
		}
		else {
			this.isAccessible = false;
			this.refreshCssClasses();
		}
	}

	/**
	 * @param {Tile} tile
	 * @param {boolean} valid
	 */
	setPreview(tile, valid) {
		this.hasPreview = true;
		this.previewTile = tile;
		this.isPreviewValid = valid;
		this.refreshCssClasses();
	}

	/**
	 * @param {Tile} tile
	 * @param {boolean} valid
	 */
	refreshPreview(valid) {
		this.isPreviewValid = valid;
		this.refreshCssClasses();
	}

	removePreview() {
		if (this.hasPreview) {
			this.hasPreview = false;
			this.previewTile = null;
			this.refreshCssClasses();
		}
	}

	/**
	 * @param {boolean=} refreshNeighbours
	 */
	refreshAccessibility(refreshNeighbours) {
		let neighbours = (this.isEmpty || refreshNeighbours) ? this.neighbours : [];

		this.isAccessible = false;
		if (this.isEmpty) {
			for (let i = 0; i < neighbours.length; i++) {
				if (neighbours[i].tile) {
					this.isAccessible = true;
				}
			}
		}
		this.refreshCssClasses();

		// Update neighbours accessibility.
		if (refreshNeighbours) {
			for (let i = 0; i < neighbours.length; i++) {
				neighbours[i].refreshAccessibility();
			}
		}
	}

	/**
	 * @param {Player} currentPlayer
	 * @param {function} playPawnCallback
	 * @param {GameContext} context
	 */
	showFeatures(currentPlayer, playPawnCallback, context) {
		this.availableFeatures = [];
		for (let i = 0; i < this.tile.features.length; i++) {
			let feature = this.tile.features[i];
			if (feature.isPlayable(currentPlayer, this, context)) {
				this.availableFeatures.push(feature);
			}
		}
	}

	hideFeatures() {
		this.availableFeatures = [];
	}

	showPawns() {
		this.occupiedFeatures = [];
		for (let i = 0; i < this.tile.features.length; i++) {
			let feature = this.tile.features[i];
			if (!feature.isFree) {
				this.occupiedFeatures.push(feature);
			}
		}
	}

	refreshPawns() {
		this.showPawns();
	}

	/**
	 * @param {Tile} tile
	 */
	onEnter(tile) {
		// Accessible empty box case.
		if (this.isEmpty && this.isAccessible && tile) {
			this.setPreview(tile, this.canBePlaced(tile));
		}
	}

	onLeave() {
		// Accessible empty box case.
		if (this.hasPreview) {
			this.removePreview();
		}
	}

	/**
	 * @param {Tile} tile
	 * @returns {boolean}
	 */
	canBePlaced(tile) {
		if (!tile) {
			return false;
		}
		let coordinates = [[this.x - 1, this.y, 'left'], [this.x + 1, this.y, 'right'], [this.x, this.y - 1, 'top'], [this.x, this.y + 1, 'bottom']];
		for (let i = 0; i < coordinates.length; i++) {
			let data = coordinates[i];
			let neighbour = Boxes.get(data[0], data[1]);
			if (neighbour && neighbour.tile && !tile.isCompatible(neighbour.tile, data[2])) {
				return false;
			}
		}
		return true;
	}

	//region DOM event listeners.
	onMouseEnter() {
		document.dispatchEvent(new CustomEvent('enterBox', { detail: { box: this } }));
	}

	onMouseLeave() {
		document.dispatchEvent(new CustomEvent('leaveBox', { detail: { box: this } }));
	}

	onPreviewRotateLeftClick() {
		this.previewTile.rotateLeft();
		this.refreshPreview(this.canBePlaced(this.previewTile));
	}

	onPreviewRotateRightClick() {
		this.previewTile.rotateRight();
		this.refreshPreview(this.canBePlaced(this.previewTile));
	}

	onPlayPreviewTile() {
		if (this.canBePlaced(this.previewTile)) {
			document.dispatchEvent(new CustomEvent('playCurrentTile', { detail: { box: this } }));
		}
	}

	/**
	 * @param {Feature} feature
	 */
	onPlayPawnOnFeature(feature) {
		document.dispatchEvent(new CustomEvent('playCurrentPawn', { detail: { box: this, feature: feature } }));
	}
	//endregion
}

let Boxes = {
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box}
	 */
	init: function(x, y) {
		let box = new Box(x, y);

		if (!Board.boxes['r' + y]) {
			Board.boxes['r' + y] = {};
		}
		Board.boxes['r' + y]['c' + x] = box;

		// Set cell classes.
		box.refreshAccessibility();
		return box;
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box}
	 */
	get: function(x, y) {
		if (!Board.boxes['r' + y] || !Board.boxes['r' + y]['c' + x]) {
			return null;
		}
		return Board.boxes['r' + y]['c' + x];
	}
};

export let Board = {
	boxes: {},
	limits: {
		top: 0,
		right: 0,
		bottom: 0,
		left: 0
	},
	rows: [0],
	columns: [0],
	/**
	 * @returns {Box}
	 */
	initFirstBox: function() {
		return Boxes.init(0, 0);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {Box|null}
	 */
	getBox: function(x, y) {
		return Boxes.get(x, y);
	},
	expandTop: function() {
		Board.limits.top--;
		Board.rows.splice(0, 0, Board.limits.top);
		for (let x = Board.limits.left; x <= Board.limits.right; x++) {
			Boxes.init(x, Board.limits.top);
		}
	},
	expandBottom: function() {
		Board.limits.bottom++;
		Board.rows.push(Board.limits.bottom);
		for (let x = Board.limits.left; x <= Board.limits.right; x++) {
			Boxes.init(x, Board.limits.bottom);
		}
	},
	expandLeft: function() {
		Board.limits.left--;
		Board.columns.splice(0, 0, Board.limits.left);
		for (let y = Board.limits.top; y <= Board.limits.bottom; y++) {
			Boxes.init(Board.limits.left, y);
		}
	},
	expandRight: function() {
		Board.limits.right++;
		Board.columns.push(Board.limits.right);
		for (let y = Board.limits.top; y <= Board.limits.bottom; y++) {
			Boxes.init(Board.limits.right, y);
		}
	},
	expand: function(x, y) {
		if (x === Board.limits.left) {
			Board.expandLeft();
		}
		if (x === Board.limits.right) {
			Board.expandRight();
		}
		if (y === Board.limits.top) {
			Board.expandTop();
		}
		if (y === Board.limits.bottom) {
			Board.expandBottom();
		}
	},
	/**
	 * @param {Box} box
	 * @param {Tile} tile
	 */
	putTile: function(box, tile) {
		box.setTile(tile);
		box.refreshAccessibility(true);

		// Expand board.
		Board.expand(box.x, box.y);
	},
	/**
	 * @param {Box} box
	 */
	removeTile: function(box) {
		box.removeTile();
		box.refreshAccessibility(true);
	},
	/**
	 * @returns {Box[]}
	 */
	getNonEmptyBoxes: function() {
		let boxes = [];
		for (let i = Board.limits.left; i <= Board.limits.right; i++) {
			for (let j = Board.limits.top; j <= Board.limits.bottom; j++) {
				let box = Board.getBox(i, j);
				if (box && !box.isEmpty) {
					boxes.push(box);
				}
			}
		}
		return boxes;
	}
};

export class Position {
	/**
	 * @param {number} x
	 * @param {number} y
	 */
	constructor(x, y) {
		/** @type {number} */
		this.x = x;
		/** @type {number} */
		this.y = y;
	}

	/**
	 * @returns {Box|null}
	 */
	get box() {
		return Boxes.get(this.x, this.y);
	}

	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {boolean}
	 */
	is(x, y) {
		return this.x === x && this.y === y;
	}
}

export class BoardZone {
	/**
	 * @param {Position[]} positions
	 */
	constructor(positions) {
		/** @type {Position[]} */
		this.positions = [];
		/** @type {Object} */
		this.coordinates = {};

		for (let i = 0; i < positions.length; i++) {
			let position = positions[i];
			let x = position.x + '';
			let y = position.y + '';
			if (!(x in this.coordinates)) {
				this.coordinates[x] = {};
			}
			if (!(y in this.coordinates[x])) { // No doubles.
				this.coordinates[x][y] = true;
				this.positions.push(position);
			}
		}
	}

	/**
	 * @returns {Box[]}
	 */
	getNonEmptyBoxes() {
		let boxes = [];
		for (let i = 0; i < this.positions.length; i++) {
			let box = this.positions[i].box;
			if (box && !box.isEmpty) {
				boxes.push(box);
			}
		}
		return boxes;
	}

	/**
	 * @returns {boolean}
	 */
	isFull() {
		for (let i = 0; i < this.positions.length; i++) {
			let box = this.positions[i].box;
			if (!box || box.isEmpty) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param {number} x
	 * @param {number} y
	 * @returns {boolean}
	 */
	contains(x, y) {
		return this.coordinates[x + ''] && this.coordinates[x + ''][y + ''] === true;
	}
}

export let Zones = {
	/**
	 * @param {ZoneData} data
	 * @param x
	 * @param y
	 */
	getZone(data, x, y) {
		switch (data.type) {
			case 'square':
				return this.getSquare(x, y, data.size);
			case 'diamond':
				return this.getDiamond(x, y, data.size);
			case 'cross':
				return this.getCross(x, y, data.size);
			case 'ix':
				return this.getIx(x, y, data.size);
			default:
				console.warn('[Zones.getZone] Unknown zone type:', data.type);
		}
		return new BoardZone([]);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getSquare(x, y, length) {
		let positions = [];
		for (let i = -length; i <= length; i++) {
			for (let j = -length; j <= length; j++) {
				positions.push(new Position(x + i, y + j));
			}
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getDiamond(x, y, length) {
		let positions = [];
		for (let i = -length; i <= length; i++) {
			let absI = Math.abs(i);
			for (let j = absI - length; j <= length - absI; j++) {
				positions.push(new Position(x - i, y - j));
			}
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getCross(x, y, length) {
		let positions = [];
		// Horizontally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y));
		}
		// Vertically.
		for (let i = -length; i <= length; i++) {
			if (i !== 0) {
				positions.push(new Position(x, y + i));
			}
		}
		return new BoardZone(positions);
	},
	/**
	 * @param {number} x
	 * @param {number} y
	 * @param {number} length
	 * @returns {BoardZone}
	 */
	getIx(x, y, length) {
		let positions = [];
		// Horizontally.
		for (let i = -length; i <= length; i++) {
			positions.push(new Position(x + i, y + i));
			if (i !== 0) {
				positions.push(new Position(x + i, y - i));
				positions.push(new Position(x - i, y + i));
				positions.push(new Position(x - i, y - i));
			}
		}
		return new BoardZone(positions);
	}
};