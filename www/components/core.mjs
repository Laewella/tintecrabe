/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
let Vue = window.Vue;

Vue.component('modal', {
	props: ['title', 'modalStyle'],
	mounted: function() {
		this.$refs.modal.focus();
	},
	template: `<transition name="modal">
			<div class="modal-mask" @click.stop="$emit(\'close\')">
				<div class="modal-wrapper">
					<div class="modal-container" :style="modalStyle" @click.stop="" @keyup.esc="$emit(\'close\')" tabindex="0" ref="modal">
						<div class="modal-header">
							<button type="button" @click="$emit(\'close\')"><img src="images/icons/close.svg" class="icon" alt="" /></button>
							<h3>{{ title }}</h3>
						</div>
						<div class="modal-body"><slot name="body">default body</slot></div>
					</div>
				</div>
			</div>
		</transition>`
});

Vue.component('confirm', {
	props: ['title', 'okLabel', 'koLabel'],
	template: `<transition name="modal">
			<div class="modal-mask">
				<div class="modal-wrapper">
					<div class="modal-container">
						<div class="modal-header">{{ title }} <button type="button" @click="$emit(\'close\')"></button></div>
						<div class="modal-body">
							<slot name="body">
								default body
							</slot>
						</div>
						<div class="modal-footer">
							<slot name="footer">
								<button class="modal-ko-button" @click="$emit(\'close\')">{{ okLabel }}</button>
								<button class="modal-ok-button" @click="$emit(\'close\')">{{ koLabel }}</button>
							</slot>
						</div>
					</div>
				</div>
			</div>
		</transition>`
});

Vue.component('tc-footer', {
	template: `<footer id="footer">
			Réalisation <a href="http://blog.darathor.net/">Darathor</a>
			|
			<a href="https://framagit.org/Darathor/tintecrabe">Code source</a>
		</footer>`
});