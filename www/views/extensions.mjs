/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { Models } from '../services/models.mjs';

export default {
	data: function() {
		return {
			Models: Models
		}
	},
	template: `<div id="view-extensions">
			<h1>Les extensions</h1>
			<div v-for="extension in Models.extensionsArray">
				<h2>
					<img v-if="extension.symbolSrc" :src="extension.symbolSrc" class="extension-symbol" alt="" />
					{{ extension.name }} <small class="text-muted">{{ extension.code }}</small>
				</h2>
				<p>{{ extension.description }}</p>
				<p v-if="extension.about">À propos : {{ extension.about }}</p>
				<h3>Pions</h3>
				<div class="pawns">
					<div v-if="!extension.pawns.length">Aucun pion.</div>
					<div v-if="extension.pawns.length">{{ extension.pawns.length }} pions :</div>
					<ul v-if="extension.pawns.length">
						<li class="item" v-for="pawn in extension.pawns">
							<tc-pawn-model-summary :model="pawn"></tc-pawn-model-summary>
						</li>
					</ul>
				</div>
				<h3>Éléments</h3>
				<div class="features">
					<div v-if="!extension.features.length">Aucun élément.</div>
					<div v-if="extension.features.length">{{ extension.features.length }} éléments :</div>
					<ul v-if="extension.features.length">
						<li class="item" v-for="feature in extension.features">
							<tc-feature-model-summary :model="feature"></tc-feature-model-summary>
						</li>
					</ul>
				</div>
				<h3>Tuiles</h3>
				<div class="tiles">
					<div v-if="!extension.tiles.length">Aucune tuile.</div>
					<div v-if="extension.tiles.length">{{ extension.tiles.length }} tuiles :</div>
					<div v-if="extension.tiles.length" class="items">
						<div class="item" v-for="tile in extension.tiles">
							<img :src="tile.src" alt="" @click="Models.Details.tile = tile"
								title="Cliquer pour voir les informations détaillées de la tuile" />
							<small class="text-muted">{{ tile.code }}</small>
						</div>
					</div>
				</div>
			</div>
		</div>`
}