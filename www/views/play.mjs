/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Game } from '../services/game.mjs';

export default {
	data: function() {
		return {
			Game: Game
		}
	},
	mounted: function() {
		this.$nextTick(function() {
			// Code that will run only after the entire view has been rendered.
			if (!Game.initialized) {
				this.$router.push('/setup');
			}
		});
	},
	template: `<div id="view-play" class="flex-horizontal">
			<div class="board">
				<tc-board></tc-board>
				<tc-player-actions></tc-player-actions>
				<tc-choose-pawn-modal v-if="Game.modals.choosePawn"></tc-choose-pawn-modal>
			</div>
			<aside class="sidebar">
				<tc-zoom></tc-zoom>
				<tc-current-tile></tc-current-tile>
				<tc-statistics></tc-statistics>
				<tc-players></tc-players>
				<tc-timeline></tc-timeline>
			</aside>
		</div>`
}