/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import { Structure } from '../services/strategies.mjs';

export class Feature {
	/** @type {Feature[]} */
	static instances = [];

	/**
	 * @param {number} id
	 * @returns {Feature|null}
	 */
	static getById(id) {
		return Feature.instances[id] || null;
	}

	/**
	 * @param {string} code
	 * @param {number} x
	 * @param {number} y
	 */
	constructor(code, x, y) {
		/** @type {number} */
		this.id = Feature.instances.length;
		Feature.instances.push(this);

		/** @type {string} */
		this.code = code;
		/** @type {number} */
		this.x = x;
		/** @type {number} */
		this.y = y;
		/** @type {Array} */
		this.pawns = [];
		/** @type {boolean} */
		this.isFree = true;
		/** @type {Feature[]} */
		this.neighbours = [];
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return this.model.name;
	}

	/**
	 * @returns {FeatureModel}
	 */
	get model() {
		let model = Models.features[this.code];
		if (!model) {
			console.error('[Feature.model] Unknown code:', this.code);
		}
		return model;
	}

	/**
	 * @param {number} rotation
	 * @returns {number}
	 */
	getX(rotation) {
		if (rotation === 0) {
			return this.x;
		}
		if (rotation === 90) {
			return 100 - this.y;
		}
		if (rotation === 180) {
			return 100 - this.x;
		}
		if (rotation === 270) {
			// noinspection JSSuspiciousNameCombination
			return this.y;
		}
	}

	/**
	 * @param {number} rotation
	 * @returns {number}
	 */
	getY(rotation) {
		if (rotation === 0) {
			return this.y;
		}
		if (rotation === 90) {
			// noinspection JSSuspiciousNameCombination
			return this.x;
		}
		if (rotation === 180) {
			return 100 - this.y;
		}
		if (rotation === 270) {
			return 100 - this.x;
		}
	}

	/**
	 * @param {Feature} neighbour
	 */
	addNeighbour(neighbour) {
		this.neighbours.push(neighbour);
	}

	/**
	 * @param {string} code
	 * @returns {Feature[]}
	 */
	getNeighboursByType(code) {
		let result = [];
		for (let i = 0; i < this.neighbours.length; i++) {
			if (this.neighbours[i].code === code) {
				result.push(this.neighbours[i]);
			}
		}
		return result;
	}

	/**
	 * @param {string[]} codes
	 * @returns {Feature[]}
	 */
	getNeighboursByTypes(codes) {
		let result = [];
		for (let i = 0; i < this.neighbours.length; i++) {
			if (ArrayUtils.contains(codes, this.neighbours[i].code)) {
				result.push(this.neighbours[i]);
			}
		}
		return result;
	}

	/**
	 * @param {Player} player
	 * @param {Box} box
	 * @param {GameContext} context
	 * @returns {boolean}
	 */
	isPlayable(player, box, context) {
		return player.getPlayablePawns(new Structure(this, box), this, context).length > 0;
	}

	/**
	 * @param {Pawn} pawn
	 */
	addPawn(pawn) {
		this.pawns.push(pawn);
		this.isFree = false;
	}

	/**
	 * @param {Pawn} pawn
	 */
	removePawn(pawn) {
		this.pawns.splice(this.pawns.indexOf(pawn), 1);
		this.isFree = this.pawns.length === 0;
	}

	removePawns() {
		ArrayUtils.clear(this.pawns);
		this.isFree = true;
	}
}

export class Tile {
	/**
	 * @param {TileModel} model
	 */
	constructor(model) {
		/** @type {TileModel} */
		this.model = model;
		/** @type {string} */
		this.code = model.code;
		/** @type {string} */
		this.src = model.src;
		/** @type {Feature[]} */
		this.features = [];
		/** @type {Feature[][]} */
		this.sides = [];
		/** @type {number} */
		this.rotation = 0;

		for (let i = 0; i < model.featuresData.length; i++) {
			let featureData = model.featuresData[i];
			this.features.push(new Feature(featureData[0], featureData[1], featureData[2]));
		}
		// Set features neighbours.
		for (let i = 0; i < model.featuresData.length; i++) {
			let neighbours = model.featuresData[i][3];
			for (let j = 0; j < neighbours.length; j++) {
				this.features[i].addNeighbour(this.features[neighbours[j]]);
			}
		}
		// Set sides.
		for (let i = 0; i < model.sides.length; i++) {
			let side = model.sides[i];
			this.sides.push([this.features[side[0]], this.features[side[1]], this.features[side[2]]]);
		}
	}

	rotateLeft() {
		this.rotation = (this.rotation <= 0) ? 270 : (this.rotation - 90);
	}

	rotateRight() {
		this.rotation = (this.rotation >= 270) ? 0 : (this.rotation + 90);
	}

	/**
	 * @param {string|number} direction top|right|bottom|left
	 * @returns {Array}
	 */
	getSide(direction) {
		let index = 4 - (this.rotation / 90);
		if (direction === 'top' || direction === 0) {
			index += 0;
		}
		else if (direction === 'right' || direction === 1) {
			index += 1;
		}
		else if (direction === 'bottom' || direction === 2) {
			index += 2;
		}
		else if (direction === 'left' || direction === 3) {
			index += 3;
		}
		return this.sides[index % 4];
	}

	/**
	 * @param {string|number} direction top|right|bottom|left
	 * @param {number} index
	 * @returns {Feature}
	 */
	getSideFeature(direction, index) {
		return this.getSide(direction)[index];
	}

	/**
	 * @param {Feature} feature
	 * @returns {number[]}
	 */
	getSideDirectionsByFeature(feature) {
		let sideDirections = [];
		for (let sideDirection = 0; sideDirection < 4; sideDirection++) {
			this.getSide(sideDirection).forEach((sideFeature) => {
				if (sideFeature === feature && !ArrayUtils.contains(sideDirections, sideDirection)) {
					sideDirections.push(sideDirection);
				}
			});
		}
		return sideDirections;
	}

	/**
	 * @param {string|number} direction
	 * @returns {number}
	 */
	getOppositeDirection(direction) {
		if (direction === 'top' || direction === 0) {
			return 2;
		}
		else if (direction === 'right' || direction === 1) {
			return 3;
		}
		else if (direction === 'bottom' || direction === 2) {
			return 0;
		}
		else if (direction === 'left' || direction === 3) {
			return 1;
		}
	}

	/**
	 * @param {number} index
	 * @returns {number}
	 */
	getOppositeIndex(index) {
		return 2 - index;
	}

	/**
	 * @param {string} direction top|right|bottom|left
	 * @param {number} index
	 * @returns {Feature}
	 */
	getOppositeFeature(direction, index) {
		return this.getSideFeature(this.getOppositeDirection(direction), this.getOppositeIndex(index));
	}

	/**
	 * @param {Tile} tile
	 * @param {string|number} position top|right|bottom|left
	 * @returns {boolean}
	 */
	isCompatible(tile, position) {
		let side1 = this.getSide(position);
		let side2 = tile.getSide(this.getOppositeDirection(position));
		return side1[0].code === side2[2].code && side1[1].code === side2[1].code && side1[2].code === side2[0].code;
	}
}