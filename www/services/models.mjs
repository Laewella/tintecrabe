/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n, ArrayUtils, FileLoader } from '../services/core.mjs';

class Extension {
	/** @type {FeatureModel[]} */
	features = [];
	/** @type {PawnModel[]} */
	pawns = [];
	/** @type {TileModel[]} */
	tiles = [];

	/**
	 * @param {string} code
	 * @param {string} symbolSrc
	 * @param {string} author
	 * @param {string} descriptionKey
	 */
	constructor(code, symbolSrc, author, descriptionKey) {
		this.code = code;
		this.author = author;
		this.descriptionKey = descriptionKey;
		this.symbolSrc = symbolSrc;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('extension_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('extension_' + this.code + '_description');
	}
}

/**
 * @typedef {Object} ZoneData
 * @property {string} type
 * @property {number} size
 */

/**
 * @typedef {Object} ConditionData
 * @property {string} type
 * @property {boolean} not
 * @property {Object} configuration
 */

/**
 * @typedef {Object} EffectData
 * @property {string} type
 * @property {ConditionData[]} conditions
 * @property {Object} configuration
 */

/**
 * @typedef {Object} PawnsData
 * @property {string[]} allowed
 * @property {ConditionData[]} conditions
 */

/**
 * @typedef {Object} CompletionData
 * @property {string} type
 * @property {Object} configuration
 */

/**
 * @typedef {Object} EvaluationData
 * @property {string} type
 * @property {ConditionData[]} conditions
 * @property {Object} configuration
 */

class PawnModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {string} type
	 * @param {number} defaultCount
	 * @param {EffectData[]} continuation
	 * @param {EvaluationData[]} majority
	 */
	constructor(code, src, type, defaultCount, continuation, majority) {
		this.code = code;
		this.src = src;
		this.type = type;
		this.defaultCount = defaultCount;
		this.continuation = continuation;
		this.majority = majority;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('pawn_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get defaultSrc() {
		return this.getSrc('black');
	}

	/**
	 * @param {string} color
	 * @returns {string}
	 */
	getSrc(color) {
		return this.src.replace('{COLOR}', color);
	}
}

class FeatureModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {PawnsData} pawns
	 * @param {EffectData[]} played
	 * @param {EffectData[]} continuation
	 * @param {CompletionData[]} completion
	 * @param {EvaluationData[]} evaluationCompleted
	 * @param {EvaluationData[]} evaluationEnd
	 */
	constructor(code, src, pawns, played, continuation, completion, evaluationCompleted, evaluationEnd) {
		this.code = code;
		this.src = src;
		this.pawns = pawns;
		this.played = played;
		this.continuation = continuation;
		this.completion = completion;
		this.evaluationCompleted = evaluationCompleted;
		this.evaluationEnd = evaluationEnd;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('feature_' + this.code);
	}
}

class TileModel {
	/**
	 * @param {string} code
	 * @param {string} src
	 * @param {Array[]} sides
	 * @param {Array[]} features
	 * @param {number} defaultQuantity
	 * @param {boolean} start
	 */
	constructor(code, src, sides, features, defaultQuantity, start) {
		this.code = code;
		this.src = src;
		this.sides = sides;
		this.featuresData = features;
		this.defaultQuantity = defaultQuantity;
		this.start = start;
	}

	/**
	 * @param {string} code
	 * @returns {boolean}
	 */
	containsFeature(code) {
		let result = false;
		this.featuresData.forEach((featureData) => {
			if (featureData[0] === code) {
				result = true;
			}
		});
		return result;
	}
}

export let Models = {
	/** @type {Extension{}} */
	extensions: {},
	/** @type {Extension[]} */
	extensionsArray: [],
	/** @type {FeatureModel{}} */
	features: {},
	/** @type {FeatureModel[]} */
	featuresArray: [],
	/** @type {PawnModel{}} */
	pawns: {},
	/** @type {PawnModel[]} */
	pawnsArray: [],
	/** @type {TileModel{}} */
	tiles: {},
	/** @type {TileModel[]} */
	tilesArray: [],
	/**
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	load: function(extensions, onDone) {
		new ExtensionLoader(I18n.LCID, extensions, onDone);
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addPawns: function(modelsData, extension) {
		modelsData.forEach((data) => {
			let pawnModel = new PawnModel(data.code, data.src, data.type, data.defaultCount || 0, data.continuation || [], data.majority || []);
			Models.pawns[data.code] = pawnModel;
			Models.pawnsArray.push(pawnModel);
			extension.pawns.push(pawnModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addFeatures: function(modelsData, extension) {
		modelsData.forEach((data) => {
			let featureModel = new FeatureModel(data.code, data.src, data.pawns || {}, data.played || [], data.continuation || [],
				data.completion || [], data.evaluationCompleted || [], data.evaluationEnd || []);
			Models.features[data.code] = featureModel;
			Models.featuresArray.push(featureModel);
			extension.features.push(featureModel);
		});
	},
	/**
	 * @param {Object[]} modelsData
	 * @param {Extension} extension
	 */
	addTiles: function(modelsData, extension) {
		modelsData.forEach((data) => {
			let tileModel = new TileModel(data.code, data.src, data.sides, data.features, data.defaultQuantity || 1, data.start || false);
			Models.tiles[data.code] = tileModel;
			Models.tilesArray.push(tileModel);
			extension.tiles.push(tileModel);
		});
	},
	/**
	 * @param {Object} response
	 */
	addExtension: function(response) {
		let extension = new Extension(response.code, response.symbolSrc, response.author, response.descriptionKey);
		Models.extensions[extension.code] = extension;
		Models.extensionsArray.push(extension);

		if ('pawns' in response) {
			Models.addPawns(response.pawns, extension);
		}
		if ('features' in response) {
			Models.addFeatures(response.features, extension);
		}
		if ('tiles' in response) {
			Models.addTiles(response.tiles, extension);
		}
	},
	/**
	 * @returns {TileModel}
	 */
	getRandomTile() {
		return ArrayUtils.randomItem(Models.tilesArray);
	},
	/**
	 * @param {string} code
	 * @returns {PawnModel}
	 */
	getPawnByCode(code) {
		return this.pawns[code];
	},
	/**
	 * @param {string} code
	 * @returns {TileModel}
	 */
	getTileByCode(code) {
		return this.tiles[code];
	}
};

/**
 * Set a tile to show the detail modal.
 * @type {Object}
 */
Models.Details = {
	tile: null
};

class ExtensionLoader {
	/**
	 * @param {string} LCID
	 * @param {string[]} extensions
	 * @param {function} onDone
	 */
	constructor(LCID, extensions, onDone) {
		let loader = new FileLoader();
		extensions.forEach((extension) => {
			loader.loadFile('extensions/' + extension + '/extension.json', this.onDefinitionLoaded, onDone);
			loader.loadFile('extensions/' + extension + '/i18n/' + LCID + '.json', (data) => { I18n.importPackage(data, ''); }, onDone);
		});
	}

	/**
	 * @param {Response} response
	 */
	onDefinitionLoaded(response) {
		Models.addExtension(response);
	}
}