/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils, Timeline, I18n } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import { Strategies, Structure } from '../services/strategies.mjs';

export class Color {
	/**
	 * @param {string} code
	 * @param {string=} cssCode
	 */
	constructor(code, cssCode) {
		this.code = code;
		this.cssCode = cssCode || code;
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('color_' + this.code);
	}
}

export let Colors = {
	black: new Color('black'),
	blue: new Color('blue'),
	green: new Color('green'),
	pink: new Color('pink'),
	red: new Color('red'),
	grey: new Color('grey'),
	violet: new Color('violet'),
	orange: new Color('orange'),
};

export class Player {
	/**
	 * @param {string=} name
	 * @param {Color=} color
	 */
	constructor(name, color) {
		this.name = name;
		this.color = color;
		this.points = 0;
		this.pawns = {};
	}

	/**
	 * @param {Pawn} pawn
	 */
	addPawn(pawn) {
		if (!this.pawns[pawn.model.code]) {
			this.pawns[pawn.model.code] = [];
		}
		this.pawns[pawn.model.code].push(pawn);
	}

	/**
	 * @param {string} code
	 * @param {number} count
	 */
	addPawns(code, count) {
		let model = Models.getPawnByCode(code);
		for (let i = 0; i < count; i++) {
			this.addPawn(new Pawn(model, this));
		}
	}

	/**
	 * @param {string} code
	 * @return {Pawn}
	 */
	popPawn(code) {
		return this.pawns[code].splice(0, 1)[0];
	}

	/**
	 * @returns {Pawn[]}
	 */
	getDifferentAvailablePawns() {
		let pawns = [];
		for (let code in this.pawns) {
			if (this.pawns[code].length) {
				pawns.push(this.pawns[code][0]);
			}
		}
		return pawns;
	}

	/**
	 * @returns {Pawn[]}
	 */
	getDifferentAvailablePawnsOfType(type) {
		let pawns = [];
		this.getDifferentAvailablePawns().forEach((pawn) => {
			if (pawn.model.type === type) {
				pawns.push(pawn);
			}
		});
		return pawns;
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {GameContext} context
	 * @returns {Pawn[]}
	 */
	getPlayablePawns(structure, feature, context) {
		let pawns = [];
		if (structure.isOccupied()) {
			if (structure.hasPlayerPawnOfType(this, 'follower')) {
				this.getDifferentAvailablePawnsOfType('helper').forEach((pawn) => {
					if (pawn.isPlayable(structure, feature, context)) {
						pawns.push(pawn);
					}
				});
			}
		}
		else {
			this.getDifferentAvailablePawnsOfType('follower').forEach((pawn) => {
				if (pawn.isPlayable(structure, feature, context)) {
					pawns.push(pawn);
				}
			});
		}
		return pawns;
	}

	/**
	 * @param {number} points
	 * @param {string} source
	 */
	addPoints(points, source) {
		this.points += Math.ceil(points);
		if (points < -1) {
			Timeline.log(this.name + ' perd ' + points + ' points ' + source + '.');
		}
		else if (points === -1) {
			Timeline.log(this.name + ' perd un point ' + source + ').');
		}
		else if (points === 0) {
			Timeline.log(this.name + ' ne marque marque aucun point ' + source + '.');
		}
		else if (points === 1) {
			Timeline.log(this.name + ' marque un point ' + source + '.');
		}
		else if (points > 1) {
			Timeline.log(this.name + ' marque ' + points + ' points ' + source + '.');
		}
	}
}

/** @type {number} */
let lastPawnId = 0;

export class Pawn {
	/**
	 * @param {PawnModel} model
	 * @param {Player} owner
	 */
	constructor(model, owner) {
		/** @type {number} */
		this.id = lastPawnId++;
		/** @type {PawnModel} */
		this.model = model;
		/** @type {Player} */
		this.owner = owner;
		/** @type {Color} */
		this.color = owner.color;
		this.src = model.getSrc(this.color.code);
	}

	/**
	 * @returns {string}
	 */
	get name() {
		let name = this.model.name;
		if (this.owner) {
			name += ' (' + this.owner.name + ')';
		}
		return name;
	}

	/**
	 * @returns {string}
	 */
	get code() {
		return this.model.code
	}

	back() {
		this.owner.addPawn(this);
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {GameContext} gameContext
	 */
	onContinued(structure, feature, gameContext) {
		this.model.continuation.forEach((strategyData) => {
			if (Strategies.checkCondition(strategyData.conditions || [], structure, gameContext.currentPlayer, gameContext.newlyPlayedPawns, this)) {
				let strategy = Strategies.getEffect(strategyData.type);
				let box = structure.getBoxByPawn(this);
				let source = '(' + this.model.name + ' [' + box.x + ', ' + box.y + '])';
				strategy.apply(structure, feature, strategyData.configuration || {}, gameContext.currentPlayer, gameContext.newlyPlayedPawns, source);
			}
		});
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {GameContext} context
	 */
	isPlayable(structure, feature, context) {
		if (!ArrayUtils.contains(feature.model.pawns.allowed || [], this.code)) {
			return false;
		}
		return Strategies.checkCondition(feature.model.pawns.conditions || [], structure, context.currentPlayer, context.newlyPlayedPawns, this);
	}
}
