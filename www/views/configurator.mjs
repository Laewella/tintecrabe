/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { I18n, ArrayUtils } from '../services/core.mjs';
import { Models } from '../services/models.mjs';

let Vue = window.Vue;

class TileConfigurator {
	/**
	 * @param {string|null} code
	 * @param {string} src
	 * @param {Array[]|null} sides
	 * @param {Array[]|null} features
	 * @param {number} defaultQuantity
	 * @param {boolean} start
	 */
	constructor(code, src, sides, features, defaultQuantity, start) {
		if (!code) {
			let parts = src.split('/');
			code = parts[parts.length - 1].split('.')[0];
		}
		this.code = code;
		this.src = src;
		this.defaultQuantity = defaultQuantity;
		this.start = start;

		if (!sides) {
			sides = [[], [], [], []];
		}
		this.sides = sides;

		if (!features) {
			features = [];
		}
		else {
			for (let i = 0; i < features.length; i++) {
				if (features[i].length === 4) {
					features[i].splice(0, 0, i);
				}
			}
		}
		this.features = features;
		this.nextKey = features.length;
	}

	/**
	 * @returns {string}
	 */
	toJSON() {
		let keys = [];
		let features = [];
		for (let i = 0; i < this.features.length; i++) {
			let feature = JSON.parse(JSON.stringify(this.features[i]));
			keys.push(feature[0]);
			features.push([feature[1], feature[2], feature[3], feature[4]]);
		}

		for (let i = 0; i < features.length; i++) {
			for (let j = 0; j < features[i][3].length; j++) {
				features[i][3][j] = keys.indexOf(features[i][3][j]);
			}
		}

		let sides = [[], [], [], []];
		for (let i = 0; i < 4; i++) {
			for (let j = 0; j < 3; j++) {
				sides[i][j] = keys.indexOf(this.sides[i][j]);
			}
		}

		return JSON.stringify({
			code: this.code,
			src: this.src,
			defaultQuantity: this.defaultQuantity,
			start: this.start,
			features: features,
			sides: sides
		})
	}

	/**
	 * @param {number} x
	 * @param {number} y
	 */
	addFeature(x, y) {
		this.features.push([this.nextKey, null, x, y, []]);
		this.nextKey++;
	}

	/**
	 * @param {Array} feature
	 */
	removeFeature(feature) {
		for (let i = 0; i < feature[4].length; i++) {
			let key = feature[4][i];
			ArrayUtils.remove(this.getFeatureByKey(key)[4], feature[0]);
		}
		ArrayUtils.remove(this.features, feature);
	}

	/**
	 * @param {number} key
	 * @returns {Array|null}
	 */
	getFeatureByKey(key) {
		for (let i = 0; i < this.features.length; i++) {
			if (this.features[i][0] === key) {
				return this.features[i];
			}
		}
		return null;
	}

	/**
	 * @param {string} code
	 * @returns {string}
	 */
	getFeatureName(code) {
		let model = Models.features[code];
		return model ? model.name : '?';
	}
}

let Data = {
	ready: false,
	Models: Models,
	featureModels: [],
	tileModels: [],
	tile: null,
	newData: {
		src: null
	},
	editData: {
		code: null
	},
	neighbours: {
		feature: null,
		/**
		 * @param {Feature} feature
		 * @returns {boolean}
		 */
		isSelected: function(feature) {
			return Data.neighbours.feature && ArrayUtils.contains(Data.neighbours.feature[4], feature[0]);
		},
		/**
		 * @param {Feature} feature
		 * @returns {string}
		 */
		getFeatureTooltip: function getFeatureTitle(feature) {
			if (feature === Data.neighbours.feature) {
				return '';
			}
			return Data.neighbours.isSelected(feature) ? 'Cliquer pour retirer des voisins' : 'Cliquer pour ajouter aux voisins';
		}
	},
	newTile: function() {
		if (Data.newData.src) {
			Data.tile = new TileConfigurator(null, Data.newData.src, null, null, 1, false);
			Data.neighbours.feature = null;
		}
		else {
			alert('URL manquante');
		}
	},
	editTile: function() {
		if (Data.editData.code) {
			let tileModel = Models.getTileByCode(Data.editData.code);
			Data.tile = new TileConfigurator(tileModel.code, tileModel.src, tileModel.sides, tileModel.featuresData, tileModel.defaultQuantity || 1, tileModel.start || false);
			Data.neighbours.feature = null;
		}
		else {
			alert('Code manquant');
		}
	},
	addFeature: function(event) {
		Data.tile.addFeature(Math.floor(event.layerX / 2), Math.floor(event.layerY / 2));
	},
	/**
	 * @param {Array} feature
	 */
	removeFeature: function(feature) {
		Data.tile.removeFeature(feature);
		if (feature === Data.neighbours.feature) {
			Data.neighbours.feature = null;
		}
	},
	/**
	 * @param {Array} feature
	 */
	editNeighbours: function(feature) {
		Data.neighbours.feature = feature;
	},
	/**
	 * @param {Array} feature1
	 */
	toggleNeighbour: function(feature1) {
		let feature2 = Data.neighbours.feature;
		if (feature2 === feature1) {
			return;
		}

		if (ArrayUtils.contains(feature2[4], feature1[0])) {
			ArrayUtils.remove(feature1[4], feature2[0]);
			ArrayUtils.remove(feature2[4], feature1[0]);
		}
		else {
			feature1[4].push(feature2[0]);
			ArrayUtils.sortNumber(feature1[4]);
			feature2[4].push(feature1[0]);
			ArrayUtils.sortNumber(feature2[4]);
		}
	}
};

let vm = new Vue({
	el: '#configurator-view',
	data: Data
});


I18n.init('fr_FR', () => {
	Models.load(['core', 'mountains', 'deserts'], () => {
		Data.ready = true;
		Data.featureModels = Models.featuresArray;
		Data.tileModels = Models.tiles;
	});
});