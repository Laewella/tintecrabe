/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Colors } from '../services/players.mjs';
import { Setup } from '../services/setup.mjs';
import { Models } from '../services/models.mjs';

let Vue = window.Vue;

Vue.component('tc-players-setup', {
	data: function() {
		return {
			Setup: Setup,
			Colors: Colors
		}
	},
	template: `<div class="tc-players-setup">
			<h2>Joueurs</h2>
			<div class="player-item" v-for="player in Setup.players">
				<label>Nom : <input type="text" v-model.trim="player.name" /></label>
				<label>Couleur : 
					<select v-model="player.color">
						<option v-for="color in Colors" :value="color" :style="{ color: color.cssCode }">{{ color.name }}</option>
					</select>
				</label>
				<button type="button" @click="Setup.removePlayer(player)" :disabled="Setup.players.length === 1">
					<img src="images/icons/delete.svg" class="icon-sm" alt="Supprimer" title="Supprimer ce joueur" />
				</button>
			</div>
			<button type="button" @click="Setup.addPlayer()">
				<img src="images/icons/add.svg" class="icon-sm" alt="" /> Ajouter un joueur
			</button>
		</div>`
});

Vue.component('tc-pawns-setup', {
	data: function() {
		return {
			pawns: Setup.pawns,
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-pawns-setup">
			<h2>Pions</h2>
			<div class="pawn-item" v-for="item in Setup.pawns">
				<label>
					<input type="number" v-model.number="item.count" min="0" max="99" step="1" size="2" />
					<tc-pawn-model-summary :code="item.code"></tc-pawn-model-summary>
				</label>
			</div>
		</div>`
});

Vue.component('tc-pool-setup', {
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-pool-setup">
			<h2>Réserve de tuiles</h2>
			<div>
				<label>
					<input type="radio" v-model="Setup.poolType" value="selection" /> Quantités sélectionnées ({{ Setup.selectedTileModels.length }})
				</label>
			</div>
			<div>
				<label>
					<input type="radio" v-model="Setup.poolType" value="random" /> Aléatoire parmi la sélection
				</label>
				<label>
					(quantité : <input type="number" v-model="Setup.poolSize" min="2" max="999" step="1" size="3" :disabled="Setup.poolType !== 'random'" />)
				</label>
			</div>
		</div>`
});

Vue.component('tc-optional-rules-setup', {
	data: function() {
		return {
			Setup: Setup
		}
	},
	template: `<div class="tc-optional-rules-setup">
			<h2>Règles optionnelles</h2>
			<div>
				<label>
					<input type="checkbox" v-model="Setup.optionalRules.extraTurnWhenFillHole" value="selection" /> Obtention d'un tour double en bouchant un trou
				</label>
			</div>
		</div>`
});

Vue.component('tc-first-tile-setup', {
	data: function() {
		return {
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-first-tile-setup">
			<h2>Choix de la tuile de départ</h2>
			<div class="tiles">
				<div class="item" v-for="tile in Setup.startTiles">
					<img :src="tile.src" alt="" @click="Models.Details.tile = tile"
						title="Cliquer pour voir les informations détaillées de la tuile" />
					<label class="text-muted"><input type="radio" v-model="Setup.startTile" :value="tile" /> {{ tile.code }} </label>
				</div>
			</div>
		</div>`
});

Vue.component('tc-tiles-list-setup', {
	data: function() {
		return {
			Setup: Setup,
			Models: Models
		}
	},
	template: `<div class="tc-tiles-list-setup">
			<h2>
				Choix des tuiles
				<button type="button" @click="Setup.selectAllTiles()" title="Sélectionner toutes les tuiles">
					<img src="images/icons/select-all.svg" class="icon-sm" alt="Tout sélectionner" />
				</button>
				<button type="button" @click="Setup.deselectAllTiles(item)" title="Dé-sélectionner toutes les tuiles">
					<img src="images/icons/deselect-all.svg" class="icon-sm" alt="Tout dé-selectionner" />
				</button>
			</h2>
			<h3>Sélection par type d'élément</h3>
			<div class="features">
				<div class="item" v-for="item in Models.featuresArray">
					<div class="image-container"><img v-if="item.src" :src="item.src" alt="" /></div>
					{{ item.name | ucfirst }}<br /><small class="text-muted">{{ item.code }}</small>
					<div>
						<button type="button" @click="Setup.selectTilesWithFeature(item)" title="Sélectionner toutes les tuiles contenant cet élément">
							<img src="images/icons/select-all.svg" class="icon-sm" alt="Tout sélectionner" />
						</button>
						<button type="button" @click="Setup.deselectTilesWithFeature(item)" title="Dé-sélectionner toutes les tuiles contenant cet élément">
							<img src="images/icons/deselect-all.svg" class="icon-sm" alt="Tout dé-selectionner" />
						</button>
					</div>
				</div>
			</div>
			<div v-for="extensionData in Setup.tilesList">
				<h3>
					<img v-if="extensionData.extension.symbolSrc" :src="extensionData.extension.symbolSrc" class="extension-symbol" alt="" />
					{{ extensionData.extension.name }} <small class="text-muted">{{ extensionData.extension.code }}</small>
					<button type="button" @click="Setup.selectExtensionTiles(extensionData)" title="Sélectionner toutes les tuiles de cette extension">
						<img src="images/icons/select-all.svg" class="icon-sm" alt="Tout sélectionner" />
					</button>
					<button type="button" @click="Setup.deselectExtensionTiles(extensionData)" title="Dé-sélectionner toutes les tuiles de cette extension">
						<img src="images/icons/deselect-all.svg" class="icon-sm" alt="Tout dé-selectionner" />
					</button>
				</h3>
				<div class="tiles">
					<div class="item" v-for="tileData in extensionData.tiles">
						<img :src="tileData.tile.src" alt="" @click="Models.Details.tile = tileData.tile"
							title="Cliquer pour voir les informations détaillées de la tuile" />
						<label class="text-muted"><input type="checkbox" v-model="tileData.selected" /> {{ tileData.tile.code }}</label>
						<input class="pull-right" type="number" v-model="tileData.quantity" min="1" max="99" step="1" size="2" :disabled="!tileData.selected" />
					</div>
				</div>
			</div>
		</div>`
});