/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { ArrayUtils, I18n } from '../services/core.mjs';
import { Strategies } from '../services/strategies.mjs';
import { Zones } from "../services/board.mjs";

class ConditionStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_condition_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_condition_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		return true;
	}
}

/**
 * Default strategy: always returns true.
 */
class NullConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.conditions.null = new NullConditionStrategy();

/**
 * Closed strategy: returns true when the structure is closed.
 */
class ClosedConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'closed';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		return structure.isClosed();
	}
}

Strategies.conditions.closed = new ClosedConditionStrategy();

/**
 * Owned by current player strategy: returns true if a pawn is given and it is owned by the given player.
 */
class OwnedPawnConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'ownedPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		return pawn && currentPlayer && currentPlayer === pawn.owner;
	}
}

Strategies.conditions.ownedPawn = new OwnedPawnConditionStrategy();

/**
 * @typedef {Object} HasPawnConditionStrategyConfiguration
 * @property {string} pawnType
 */

/**
 * Has pawn strategy: returns true if the player has a pawn of the given type in the structure.
 */
class HasPawnConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'hasPawn';
	}

	/**
	 * @param {Structure} structure
	 * @param {HasPawnConditionStrategyConfiguration} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		return structure.hasPlayerPawnOfCode(pawn ? pawn.owner : currentPlayer, configuration.pawnType);
	}
}

Strategies.conditions.hasPawn = new HasPawnConditionStrategy();

/**
 * Already present strategy: returns true if a pawn is given and it was already present before this turn.
 */
class AlreadyPresentConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'alreadyPresent';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		return pawn && !ArrayUtils.contains(newlyPlayedPawns, pawn);
	}
}

Strategies.conditions.alreadyPresent = new AlreadyPresentConditionStrategy();

/**
 * @typedef {Object} FeatureInZoneConditionStrategyConfiguration
 * @property {string} featureType
 * @property {ZoneData} zone
 */

/**
 * Feature in zone strategy: returns true a zone centered on the structure's initial box contains a feature of a given type.
 */
class FeatureInZoneConditionStrategy extends ConditionStrategy {
	constructor() {
		super();
		this.code = 'featureInZone';
	}

	/**
	 * @param {Structure} structure
	 * @param {FeatureInZoneConditionStrategyConfiguration} configuration
	 * @param {Player=} currentPlayer
	 * @param {Pawn[]=} newlyPlayedPawns
	 * @param {Pawn=} pawn
	 * @returns {boolean}
	 */
	evaluate(structure, configuration, currentPlayer, newlyPlayedPawns, pawn) {
		let result = false;
		Zones.getZone(configuration.zone, structure.initialBox.x, structure.initialBox.y).getNonEmptyBoxes().forEach((box) => {
			box.tile.features.forEach((feature) => {
				if (feature.code === configuration.featureType) {
					result = true;
				}
			});
		});
		return result;
	}
}

Strategies.conditions.featureInZone = new FeatureInZoneConditionStrategy();