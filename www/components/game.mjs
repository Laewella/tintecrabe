/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n, Timeline } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import { Board, Zoom } from '../services/board.mjs';
import { Game, TilesPool } from '../services/game.mjs';

let Vue = window.Vue;

Vue.component('tc-choose-pawn-modal', {
	data: function() {
		let player = Game.modals.choosePawn.player;
		return {
			player: player,
			feature: Game.modals.choosePawn.feature,
			pawns: Game.modals.choosePawn.pawns,
			/**
			 * @param {Pawn} pawn
			 */
			playPawn(pawn) {
				Game.playPawn(Game.modals.choosePawn.feature, Game.modals.choosePawn.box, player.popPawn(pawn.code), player);
				this.close();
			},
			close() {
				Game.modals.choosePawn = null;
			}
		}
	},
	template: `<modal :title="'c.main.which_pawn_to_place' | trans" @close="close()">
			<div slot="body">
				<span v-for="pawn in pawns">
				 <button @click="playPawn(pawn)"><tc-pawn :pawn="pawn" size="26"></tc-pawn></button>
				</span>
			</div>
		</modal>`
});

Vue.component('tc-zoom', {
	data: function() {
		return {
			Zoom: Zoom
		};
	},
	template: `<div class="tc-zoom">
			{{ 'c.main.zoom' | trans(['lab']) }}
			<button id="zoom-x1" type="button" @click="Zoom.set(1)" :disabled="Zoom.level == 1">x1</button>
			<button id="zoom-x2" type="button" @click="Zoom.set(2)" :disabled="Zoom.level == 2">x2</button>
			<button id="zoom-x4" type="button" @click="Zoom.set(4)" :disabled="Zoom.level == 4">x4</button>
		</div>`
});

Vue.component('tc-current-tile', {
	data: function() {
		return {
			Game: Game,
			showTileDetails: function() {
				Models.Details.tile = Game.currentTile.model;
			}
		};
	},
	computed: {
		tile: function() {
			return Game.currentTile
		}
	},
	template: `<div class="tc-current-tile" v-if="tile">
			<div>{{ 'c.main.current_tile' | trans(['lab']) }}</div>
			<div class="actions">
				<button @click="tile.rotateLeft()" :title="'c.main.rotate_left' | trans">
					<img src="images/icons/rotate-left.svg" class="icon icon-lg" alt="" />
				</button>
				<button @click="tile.rotateRight()" :title="'c.main.rotate_right' | trans">
					<img src="images/icons/rotate-left.svg" class="icon icon-lg flip-horizontal" alt="" />
				</button>
				<button @click="showTileDetails()">
					<img src="images/icons/info.svg" class="icon icon-lg" alt="" />
				</button>
			</div>
			<img :src="tile.src" :alt="tile.code" :class="'tile rotate-' + tile.rotation" @click="showTileDetails()" class="clickable" />
		</div>`
});

Vue.component('tc-statistics', {
	data: function() {
		return {
			pool: TilesPool.pool
		};
	},
	template: `<div class="tc-statistics">
			<dl>
				<dt>{{ 'c.main.remaining_tiles' | trans(['lab']) }}</dt><dd>{{ pool.length }}</dd>
			</dl>
		</div>`
});

Vue.component('tc-players', {
	data: function() {
		return {
			Game: Game
		};
	},
	template: `<div class="tc-players">
			{{ 'c.main.players' | trans(['lab']) }}
			<ul>
				<li v-for="(player, index) in Game.players" :class="index === Game.currentPlayerIndex ? 'current' : ''">
					<span class="bullet" :style="{background: player.color.cssCode}"></span>
					<span class="name">{{ player.name }}</span> ({{ player.points }})
					<div class="player-pawns">
						<span v-for="pawnCategory in player.pawns">
							<tc-pawn v-for="pawn in pawnCategory" :key="pawn.id" :pawn="pawn" size="26"></tc-pawn>
						</span>
					</div>
				</li>
			</ul>
		</div>`
});

Vue.component('tc-timeline', {
	data: function() {
		return {
			items: Timeline.items
		};
	},
	template: `<div class="tc-timeline">
			{{ 'c.main.history' | trans(['lab']) }}
			<ul>
				<li v-for="item in items">
					<span class="time">{{ item.date | time }}</span> <span class="message">{{ item.message }}</span>
				</li>
			</ul>
		</div>`
});

Vue.component('tc-player-actions', {
	data: function() {
		return {
			Game: Game
		};
	},
	computed: {
		visible: function() {
			return Game.turnStep === 'playPawn' || Game.turnStep === 'endMainPhase';
		},
		inPlayPawnPhase: function() {
			return Game.turnStep === 'playPawn';
		},
		inEndMainPhase: function() {
			return Game.turnStep === 'endMainPhase';
		}
	},
	template: `<div class="tc-player-actions" v-if="visible">
			<div v-if="inPlayPawnPhase">
				<button type="button" @click="Game.undoPlayTile()">{{ 'c.main.cancel' | trans }}</button>
				<button type="button" @click="Game.checkStructures()">{{ 'c.main.do_nothing' | trans }}</button>
			</div>
			<div v-if="inEndMainPhase">
				<button type="button" @click="Game.undoPlayPawn()">{{ 'c.main.cancel' | trans }}</button>
				<button type="button" @click="Game.checkStructures()">{{ 'c.main.confirm' | trans }}</button>
			</div>
		</div>`
});

Vue.component('tc-pawn', {
	props: ['pawn', 'size'],
	template: `<span :class="['pawn', this.size ? 'pawn-' + this.size : null]">
			<img :alt="pawn.name | ucfirst" :title="pawn.name | ucfirst" :src="pawn.src" />
		</span>`
});

Vue.component('tc-board-box', {
	props: ['x', 'y'],
	data: function() {
		return {
			box: Board.getBox(this.x, this.y)
		}
	},
	watch: {
		x: function() {
			this.box = Board.getBox(this.x, this.y);
		},
		y: function() {
			this.box = Board.getBox(this.x, this.y);
		}
	},
	template: `<td v-if="box" :class="['tc-board-box', this.box.cssClasses]" @mouseenter="box.onMouseEnter()" @mouseleave="box.onMouseLeave()">
			<div v-if="box.hasPreview">
				<img :src="box.previewTile.src" :class="['tile', 'rotate-' + box.previewTile.rotation]"
					@click.stop="box.onPlayPreviewTile()" />
				<img src="images/icons/rotate-left.svg" :title="'c.main.rotate_left' | trans"
					class="preview-button preview-button-rotate-left icon" @click.stop="box.onPreviewRotateLeftClick()" />
				<img src="images/icons/rotate-left.svg" :title="'c.main.rotate_right' | trans"
					class="preview-button preview-button-rotate-right icon flip-horizontal" @click.stop="box.onPreviewRotateRightClick()" />
			</div>
			<div v-if="!box.isEmpty">
				<img :src="box.tile.src" :class="['tile', 'rotate-' + box.tile.rotation]" />
				<span v-for="feature in box.availableFeatures" class="feature accessible" :title="feature.name"
					:style="{left: feature.getX(box.tile.rotation) + '%', top: feature.getY(box.tile.rotation) + '%'}"
					@click.stop="box.onPlayPawnOnFeature(feature)"></span>
				<span v-for="feature in box.occupiedFeatures" class="feature occupied"
					:style="{left: feature.getX(box.tile.rotation) + '%', top: feature.getY(box.tile.rotation) + '%'}">
					<tc-pawn v-for="pawn in feature.pawns" :key="pawn.id" :pawn="pawn"></tc-pawn>
				</span>
			</div>
		</td>`
});

Vue.component('tc-board', {
	data: function() {
		return {
			Zoom: Zoom,
			Board: Board
		};
	},
	template: `<table class="tc-board" :class="'zoom-x' + Zoom.level">
			<thead>
				<tr>
					<th class="corner"></th>
					<th class="label" v-for="x in Board.columns">{{ x }}</th>
					<th class="corner"></th>
				</tr>
			</thead>
			<tbody>
				<tr v-for="y in Board.rows">
					<th class="label">{{ y }}</th>
						<tc-board-box v-for="x in Board.columns" :x="x" :y="y" :key="x + '_' + y"></tc-board-box>
					<th class="label">{{ y }}</th>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<th class="corner"></th>
					<th class="label" v-for="x in Board.columns">{{ x }}</th>
					<th class="corner"></th>
				</tr>
			</tfoot>
		</table>`
});