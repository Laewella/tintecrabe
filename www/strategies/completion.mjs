/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from '../services/core.mjs';
import { Zones } from '../services/board.mjs';
import { Strategies } from '../services/strategies.mjs';

class CompletionStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_completion_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_completion_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @returns {boolean}
	 */
	evaluate(structure, configuration) {
		return true;
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 */
	register(structure, configuration) {
		// Nothing by default.
	}
}

/**
 * Default strategy: never completed.
 */
class NullCompletionStrategy extends CompletionStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.completion.null = new NullCompletionStrategy();

/**
 * Closed strategy: completed when the structure is closed.
 */
class ClosedCompletionStrategy extends CompletionStrategy {
	constructor() {
		super();
		this.code = 'closed';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @returns {boolean}
	 */
	evaluate(structure, configuration) {
		return structure.isClosed();
	}
}

Strategies.completion.closed = new ClosedCompletionStrategy();

/**
 * @typedef {Object} AdjacentCompletionStrategyConfiguration
 * @property {string} featureType - Indicates whether the Power component is present.
 * @property {string} featureStatus - feature status close|open|any.
 */

/**
 * AdjacentClosed strategy: completed when each adjacent structure with the required type are matching a given status.
 * Parameters:
 *  - featureType: the feature type to count.
 *  - featureStatus: feature status close|open|any.
 */
class AdjacentCompletionStrategy extends CompletionStrategy {
	constructor() {
		super();
		this.code = 'adjacent';
	}

	/**
	 * @param {Structure} structure
	 * @param {AdjacentCompletionStrategyConfiguration} configuration
	 * @returns {boolean}
	 */
	evaluate(structure, configuration) {
		let result = true;
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach(function(neighbour) {
			if (!neighbour.checkStatus(configuration.featureStatus)) {
				result = false;
			}
		});
		return result;
	}

	/**
	 * @param {Structure} structure
	 * @param {AdjacentCompletionStrategyConfiguration} configuration
	 */
	register(structure, configuration) {
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach(function(neighbour) {
			if (!neighbour.checkStatus(configuration.featureStatus)) {
				Strategies.registerFeatureNeighbour(structure.initialFeature, structure.initialBox, neighbour.initialFeature);
			}
		});
	}
}

Strategies.completion.adjacent = new AdjacentCompletionStrategy();

/**
 * Square strategy: completed when a square centered on the feature has all its boxes filled.
 * Parameters:
 *  - length: the square tile count from the center.
 */
class SquareCompletionStrategy extends CompletionStrategy {
	constructor() {
		super();
		this.code = 'square';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @returns {boolean}
	 */
	evaluate(structure, configuration) {
		return Zones.getSquare(structure.initialX, structure.initialY, configuration.length).isFull();
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 */
	register(structure, configuration) {
		let positions = Zones.getSquare(structure.initialX, structure.initialY, configuration.length).positions;
		Strategies.registerFeature(structure.initialFeature, structure.initialBox, positions);
	}
}

Strategies.completion.square = new SquareCompletionStrategy();

/**
 * Diamond strategy: completed when a "diamond" centered on the feature has all its boxes filled.
 * Parameters:
 *  - length: the diamond tile count from the center (horizontally and vertically).
 */
class DiamondCompletionStrategy extends CompletionStrategy {
	constructor() {
		super();
		this.code = 'diamond';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @returns {boolean}
	 */
	evaluate(structure, configuration) {
		return Zones.getDiamond(structure.initialX, structure.initialY, configuration.length).isFull();
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 */
	register(structure, configuration) {
		let positions = Zones.getDiamond(structure.initialX, structure.initialY, configuration.length).positions;
		Strategies.registerFeature(structure.initialFeature, structure.initialBox, positions);
	}
}

Strategies.completion.diamond = new DiamondCompletionStrategy();