/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import '../strategies/all.mjs';
import { I18n, ArrayUtils, Timeline } from '../services/core.mjs';
import { Board } from '../services/board.mjs';
import { Feature, Tile } from '../services/tiles.mjs';
import { GameContext, Strategies, Structure } from '../services/strategies.mjs';
import { Player, Pawn } from '../services/players.mjs';
import { Setup } from '../services/setup.mjs';

let document = window.document;

export let TilesPool = {
	/** @type {Tile[]} */
	pool: [],
	/** @type {Tile} */
	startTile: null,
	init: function() {
		this.startTile = new Tile(Setup.startTile);

		if (Setup.poolType === 'selection') {
			Setup.selectedTileModels.forEach((model) => {
				this.pool.push(new Tile(model));
			});
		}
		else if (Setup.poolType === 'random') {
			for (let i = 0; i < Setup.poolSize; i++) {
				this.pool.push(new Tile(ArrayUtils.randomItem(Setup.selectedTileModels)));
			}
		}
		else {
			console.error('[TilesPool.init] Unknown poolType:', Setup.poolType);
		}
	},
	/**
	 * @returns {boolean}
	 */
	isEmpty: function() {
		return TilesPool.pool.length === 0;
	},
	/**
	 * @returns {Tile}
	 */
	draw: function() {
		return TilesPool.pool.splice(ArrayUtils.randomIndex(TilesPool.pool), 1)[0];
	}
};

/**
 * @typedef {Object} PlayedPawn
 * @property {Pawn} pawn
 * @property {Feature} feature
 * @property {Box} box
 */

/**
 * @typedef {Object} OptionalRules
 * @property {boolean} extraTurnWhenFillHole
 */

export let Game = {
	/** @type {boolean} */
	initialized: false,
	/** @type {Player[]} */
	players: [],
	/** @type {OptionalRules} */
	optionalRules: {},
	/** @type {number|null} */
	currentPlayerIndex: null,
	/** @type {Player|null} */
	currentPlayer: null,
	/** @type {Tile|null} */
	currentTile: null,
	/** @type {Box|null} */
	lastPlayedBox: null,
	/** @type {PlayedPawn[]} */
	lastPlayedPawns: [],
	/** @type {boolean} */
	hasExtraTurn: false,
	/** @type {boolean} */
	isExtraTurn: false,
	/** @type {Object} */
	stepsData: {},
	/** @type {string} */
	turnStep: 'begin',
	modals: {
		/** @type {Object|null} */
		choosePawn: null
	},
	/**
	 * @returns {Pawn[]}
	 */
	get newlyPlayedPawns() {
		let newlyPlayedPawns = [];
		this.lastPlayedPawns.forEach((playedPawn) => {
			newlyPlayedPawns.push(playedPawn.pawn);
		});
		return newlyPlayedPawns;
	},
	/**
	 * @returns {GameContext}
	 */
	get gameContext() {
		return new GameContext(this.currentPlayer, this.newlyPlayedPawns, [this.lastPlayedBox]);
	},
	/**
	 * @param {string} source
	 */
	gainExtraTurn(source) {
		if (!this.hasExtraTurn) {
			this.hasExtraTurn = true;
			if (source) {
				Timeline.log(this.currentPlayer.name + ' obtient un tour double ' + source + '.');
			}
		}
		else {
			this.currentPlayer.addPoints(2, source);
		}
	},
	/**
	 * @param {Box} box
	 */
	playTile: function(box) {
		if (!Game.currentTile) {
			return;
		}

		Board.putTile(box, Game.currentTile);
		Game.currentTile = null;
		Game.lastPlayedBox = box;
		box.showFeatures(Game.currentPlayer, Game.playPawn, this.gameContext);

		// Timeline.
		Timeline.log('Tuile placée en [' + box.x + ', ' + box.y + '].');

		Game.turnStep = 'playPawn';
	},
	undoPlayTile: function() {
		if (Game.turnStep !== 'playPawn') {
			console.error('[Game.undoPlayTile] Invalid step:', Game.turnStep);
			return;
		}

		let box = Game.lastPlayedBox;
		Game.currentTile = box.tile;
		Board.removeTile(box);
		Game.lastPlayedBox = null;
		box.hideFeatures();

		// Timeline.
		Timeline.log('Tuile reprise.');

		Game.turnStep = 'playTile';
	},
	/**
	 * @param {Feature} feature
	 * @param {Box} box
	 * @param {Pawn=} pawn
	 * @param {Player=} player
	 */
	playPawn: function(feature, box, pawn, player) {
		if (Game.turnStep !== 'playPawn') {
			console.error('[Game.playPawn] Invalid step:', Game.turnStep);
			return;
		}

		if (!player) {
			player = Game.currentPlayer;
		}

		if (!pawn) {
			let pawns = player.getPlayablePawns(new Structure(feature, box), feature, this.gameContext);
			if (pawns.length === 0) {
				return;
			}
			if (pawns.length > 1) {
				Game.modals.choosePawn = {
					player: player,
					feature: feature,
					box: box,
					pawns: pawns
				};
				return;
			}

			pawn = player.popPawn(pawns[0].code);
		}

		feature.addPawn(pawn);
		box.showPawns();
		box.hideFeatures();

		Game.lastPlayedPawns.push({ pawn: pawn, feature: feature, box: box });

		Game.turnStep = 'endMainPhase';
	},
	undoPlayPawn: function() {
		if (Game.turnStep !== 'endMainPhase') {
			console.error('[Game.undoPlayPawn] Invalid step:', Game.turnStep);
			return;
		}

		for (let i = 0; i < Game.lastPlayedPawns.length; i++) {
			let pawn = Game.lastPlayedPawns[i].pawn;
			let feature = Game.lastPlayedPawns[i].feature;
			let box = Game.lastPlayedPawns[i].box;
			feature.removePawn(pawn);
			pawn.back();
			box.showFeatures(Game.currentPlayer, Game.playPawn, this.gameContext);
			box.refreshPawns();
		}
		Game.lastPlayedPawns = [];
		Game.turnStep = 'playPawn';
	},
	checkStructures: function() {
		if (Game.turnStep !== 'playPawn' && Game.turnStep !== 'endMainPhase') {
			console.error('[Game.checkStructures] Invalid step:', Game.turnStep);
			return;
		}

		let box = Game.lastPlayedBox;

		// Handle "played" effects.
		box.tile.features.forEach((feature) => {
			feature.model.played.forEach((playedData) => {
				let structure = new Structure(feature, box);
				if (Strategies.checkCondition(playedData.conditions || [], structure, this.currentPlayer, this.newlyPlayedPawns)) {
					let strategy = Strategies.getEffect(playedData.type);
					let source = '(' + I18n.trans('c.main.triggered_by', ['lab']) + ' ' + feature.name + ' [' + box.x + ', ' + box.y + '])';
					strategy.apply(structure, feature, playedData.configuration || {}, this.currentPlayer, this.newlyPlayedPawns, source);
				}
			});
		});

		// Handle completed/continued structures effects.
		let alreadyTestedFeatures = [];

		Strategies.getRegisteredFeatures(box.x, box.y).forEach((registeredFeature) => {
			if (alreadyTestedFeatures.indexOf(registeredFeature[0]) !== -1) {
				return;
			}

			let structure = new Structure(registeredFeature[0], registeredFeature[1]);
			ArrayUtils.merge(alreadyTestedFeatures, structure.features);

			structure.checkCompletion();
		});

		box.tile.features.forEach((feature) => {
			let structure = new Structure(feature, box);

			// Check continued structure.
			if (structure.features.length > 0) {
				structure.onContinued(feature, this.gameContext);
			}

			// Check completed structures.
			if (alreadyTestedFeatures.indexOf(feature) !== -1) {
				return;
			}
			ArrayUtils.merge(alreadyTestedFeatures, structure.features);

			if (structure.isCompleted()) {
				structure.onCompleted(feature, this.currentPlayer, this.newlyPlayedPawns);
			}
			else {
				structure.register();
			}
		});

		// Fill a hole gives an extra turn.
		if (this.optionalRules.extraTurnWhenFillHole) {
			let fillHole = true;
			box.neighbours.forEach((neighbour) => {
				if (neighbour.isEmpty) {
					fillHole = false;
				}
			});
			if (fillHole) {
				this.gainExtraTurn('(trou bouché [' + box.x + ', ' + box.y + '])');
			}
		}

		Game.turnStep = 'end';
		Game.endTurn();
	},
	endTurn: function() {
		if (Game.turnStep !== 'end') {
			console.error('[Game.endTurn] Invalid step:', Game.turnStep);
			return;
		}

		Game.lastPlayedBox.hideFeatures();

		// Clear data.
		Game.currentTile = null;
		Game.lastPlayedBox = null;
		Game.lastPlayedPawns = [];

		// Check end.
		if (TilesPool.isEmpty()) {
			Game.endGame();
		}
		// Check extra turn.
		else if (this.hasExtraTurn && !this.isExtraTurn) {
			this.isExtraTurn = true;

			Game.startTurn();
		}
		// Next player.
		else {
			this.hasExtraTurn = false;
			this.isExtraTurn = false;

			// Next player.
			Game.currentPlayerIndex = ArrayUtils.nextIndex(Game.players, Game.currentPlayerIndex);
			Game.currentPlayer = Game.players[Game.currentPlayerIndex];

			Game.startTurn();
		}
	},
	startTurn: function() {
		Game.turnStep = 'start';

		// Reset step data.
		Game.stepsData = {
			begin: {},
			playTile: {},
			playPawn: {},
			end: {}
		};

		// Draw tile.
		Game.currentTile = TilesPool.draw();

		Game.turnStep = 'playTile';
		console.log('------------ START TURN ------------');
	},
	init: function() {
		Game.initialized = true;

		Setup.saveLast();

		TilesPool.init(Setup.poolSize);

		Game.players = Setup.players;
		Game.optionalRules = Setup.optionalRules;

		// Init pawns.
		Setup.pawns.forEach(function(item) {
			Game.players.forEach(function(player) {
				player.addPawns(item.code, item.count);
			});
		});

		// Push the first tile.
		let box = Board.initFirstBox();
		Board.putTile(box, TilesPool.startTile);

		// Register features from initial tile.
		let tileFeatures = box.tile.features;
		for (let i = 0; i < tileFeatures.length; i++) {
			let structure = new Structure(tileFeatures[i], box);
			if (!structure.isCompleted()) {
				structure.register();
			}
		}

		// Select first player.
		Game.currentPlayerIndex = ArrayUtils.randomIndex(Game.players);
		Game.currentPlayer = Game.players[Game.currentPlayerIndex];

		Game.startTurn();
		Timeline.log('La partie commence !');
	},
	endGame: function() {
		Timeline.log('Décompte final des points');

		let alreadyTestedFeatures = [];

		Board.getNonEmptyBoxes().forEach(function(box) {
			box.tile.features.forEach(function(feature) {
				if (ArrayUtils.contains(alreadyTestedFeatures, feature)) {
					return;
				}

				let structure = new Structure(feature, box);
				ArrayUtils.merge(alreadyTestedFeatures, structure.features);

				structure.onEndGame();
			});
		});

		Timeline.log('La partie est finie !');
	}
};

//region Event listeners on board events.
document.addEventListener('playCurrentTile', function(event) {
	Game.playTile(event.detail.box);
});

document.addEventListener('playCurrentPawn', function(event) {
	Game.playPawn(event.detail.feature, event.detail.box);
});

document.addEventListener('giveExtraTurn', function(event) {
	Game.gainExtraTurn(event.detail.source);
});

document.addEventListener('scorePoints', function(event) {
	Game.currentPlayer.addPoints(event.detail.points, event.detail.source);
});

document.addEventListener('enterBox', function(event) {
	event.detail.box.onEnter(Game.currentTile);
});

document.addEventListener('leaveBox', function(event) {
	event.detail.box.onLeave();
});
//endregion