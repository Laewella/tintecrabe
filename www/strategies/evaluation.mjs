/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n, ArrayUtils } from '../services/core.mjs';
import { Zones } from '../services/board.mjs';
import { Strategies, Structure } from '../services/strategies.mjs';

class EvaluationStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_evaluation_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_evaluation_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		return 0;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @param {Structure} structure
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @returns {number}
	 */
	evaluateBoxes(boxes, configuration, structure, player, pawns) {
		if (configuration.type === 'tiles') {
			return this.evaluateBoxesTiles(boxes, configuration);
		}
		if (configuration.type === 'features') {
			return this.evaluateBoxesFeatures(boxes, configuration, structure);
		}
		console.warn('[AbstractAreaEvaluationStrategy.evaluateBoxes] Unknown type:', configuration.type);
		return 0;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @returns {number}
	 */
	evaluateBoxesTiles(boxes, configuration) {
		return boxes.length * configuration.value;
	}

	/**
	 * @param {Box[]} boxes
	 * @param {Object} configuration
	 * @param {Structure} structure
	 * @returns {number}
	 */
	evaluateBoxesFeatures(boxes, configuration, structure) {
		let result = 0;
		let alreadyTestedFeatures = [structure.features];
		for (let boxIndex = 0; boxIndex < boxes.length; boxIndex++) {
			let box = boxes[boxIndex];
			let features = box.tile.features;
			for (let i = 0; i < features.length; i++) {
				if (features[i].model.code === configuration.featureType && !ArrayUtils.contains(alreadyTestedFeatures, features[i])) {
					let structure = new Structure(features[i], box);
					ArrayUtils.merge(alreadyTestedFeatures, structure.features);
					if (structure.checkStatus(configuration.featureStatus)) {
						result += configuration.value;
					}
				}
			}
		}
		return result;
	}
}

/**
 * Default strategy: always 0 points.
 */
class NullEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.evaluation.null = new NullEvaluationStrategy();

/**
 * Fixed strategy: a fixed points number.
 * Parameters:
 *  - value: points
 */
class FixedEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'fixed';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		return configuration.value;
	}
}

Strategies.evaluation.fixed = new FixedEvaluationStrategy();

/**
 * Tiles strategy: n points for each tile in the structure.
 * Parameters:
 *  - value: points per tile
 */
class TilesEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'tiles';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		return this.evaluateBoxesTiles(structure.boxes, configuration);
	}
}

Strategies.evaluation.tiles = new TilesEvaluationStrategy();

/**
 * @typedef {Object} AdjacentEvaluationStrategyConfiguration
 * @property {number} value - points per structure.
 * @property {string} featureType - Indicates whether the Power component is present.
 * @property {string} featureStatus - feature status close|open|any.
 */

/**
 * Adjacent strategy: n points for each adjacent structure of a given type.
 * Parameters:
 *  - value: points per structure.
 *  - featureType: the feature type to count.
 *  - featureStatus: feature status close|open|any.
 */
class AdjacentEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'adjacent';
	}

	/**
	 * @param {Structure} structure
	 * @param {AdjacentEvaluationStrategyConfiguration} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		let result = 0;
		let neighbours = structure.getNeighbourStructuresByTypes([configuration.featureType]);
		neighbours.forEach(function(neighbour) {
			if (neighbour.checkStatus(configuration.featureStatus)) {
				result += configuration.value;
			}
		});
		return result;
	}
}

Strategies.evaluation.adjacent = new AdjacentEvaluationStrategy();

/**
 * Square strategy: n points for each element in a square centered on the feature.
 * Parameters:
 *  - length: the square tile count from the center.
 *  - type: element checking mode.
 *  - value: points per element.
 * Parameters for type:
 *  - features:
 *    - featureType: the feature type to count.
 *    - featureStatus: feature status close|open|any.
 */
class SquareEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'square';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		let boxes = Zones.getSquare(structure.initialX, structure.initialY, configuration.length).getNonEmptyBoxes();
		return this.evaluateBoxes(boxes, configuration, structure, player, pawns);
	}
}

Strategies.evaluation.square = new SquareEvaluationStrategy();

/**
 * Diamond strategy: n points for each element in a "diamond" centered on the feature has all its boxes filled.
 * Parameters:
 *  - length: the diamond tile count from the center (horizontally and vertically).
 *  - type: element checking mode.
 *  - value: points per element.
 * Parameters for type:
 *  - feature:
 *    - featureType: the feature type to count.
 *    - featureStatus: feature status close|open|any.
 */
class DiamondEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'diamond';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		let boxes = Zones.getDiamond(structure.initialX, structure.initialY, configuration.length).getNonEmptyBoxes();
		return this.evaluateBoxes(boxes, configuration, structure, player, pawns);
	}
}

Strategies.evaluation.diamond = new DiamondEvaluationStrategy();

/**
 * Connections strategy: score points depending on the number of connections between the last played feature and the existing ones in the structure.
 */
class ConnectionsEvaluationStrategy extends EvaluationStrategy {
	constructor() {
		super();
		this.code = 'connections';
	}

	/**
	 * @param {Structure} structure
	 * @param {Object} configuration
	 * @param {Player=} player
	 * @param {Pawn[]=} pawns
	 * @param {Feature=} lastFeature
	 * @returns {number}
	 */
	evaluate(structure, configuration, player, pawns, lastFeature) {
		if (!lastFeature) {
			return 0;
		}

		return configuration.thresholds[structure.getConnectionsCount(lastFeature)];
	}
}

Strategies.evaluation.connections = new ConnectionsEvaluationStrategy();