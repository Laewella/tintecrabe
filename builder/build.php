<?php
$colors = [
	'black',
	'blue',
	'green',
	'pink',
	'red',
	'grey',
	'violet',
	'orange'
];

$templates = [
	'follower',
	'phantom',
	'mayor',
	'builder',
	'pig',
	'waggon',
	'super'
];

foreach ($templates as $template)
{
	$content = file_get_contents('templates' . DIRECTORY_SEPARATOR . $template . '.svg');
	$dir = 'generated' . DIRECTORY_SEPARATOR . $template;
	if (!is_dir($dir) && !mkdir($dir) && !is_dir($dir))
	{
		throw new RuntimeException(sprintf('Directory "%s" was not created', $dir));
	}
	foreach ($colors as $color)
	{
		file_put_contents($dir . DIRECTORY_SEPARATOR . $color . '.svg', str_replace('{{COLOR}}', $color, $content));
	}
}