/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { I18n } from '../services/core.mjs';
import { Structure, StructureEvaluator, Strategies } from '../services/strategies.mjs';
import { Zones } from "../services/board.mjs";

class EffectStrategy {
	constructor() {
		/** @type {string} */
		this.code = '';
	}

	/**
	 * @returns {string}
	 */
	get name() {
		return I18n.trans('strategy_effect_' + this.code);
	}

	/**
	 * @returns {string}
	 */
	get description() {
		return I18n.trans('strategy_effect_' + this.code + '_description');
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Player} currentPlayer
	 * @param {Pawn[]} newlyPlayedPawns
	 * @param {string} source
	 */
	apply(structure, feature, configuration, currentPlayer, newlyPlayedPawns, source) {
		// Do nothing by default.
	}
}

/**
 * Default strategy: no effect.
 */
class NullEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'null';
	}
}

Strategies.effects.null = new NullEffectStrategy();

/**
 * Extra turn strategy: give an extra turn to the current player.
 */
class ExtraTurnEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'extraTurn';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Player} currentPlayer
	 * @param {Pawn[]} newlyPlayedPawns
	 * @param {string} source
	 */
	apply(structure, feature, configuration, currentPlayer, newlyPlayedPawns, source) {
		document.dispatchEvent(new CustomEvent('giveExtraTurn', { detail: { source: source } }));
	}
}

Strategies.effects.extraTurn = new ExtraTurnEffectStrategy();

/**
 * @typedef {Object} ScorePointsEffectStrategyConfiguration
 * @property {ScorePointsEffectStrategyConfigurationStrategy} strategy
 */

/**
 * @typedef {Object} ScorePointsEffectStrategyConfigurationStrategy
 * @property {string} type
 * @property {Object} configuration
 */

/**
 * Extra turn strategy: give an extra turn to the current player.
 */
class ScorePointsEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'scorePoints';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Player} currentPlayer
	 * @param {Pawn[]} newlyPlayedPawns
	 * @param {string} source
	 */
	apply(structure, feature, configuration, currentPlayer, newlyPlayedPawns, source) {
		let strategy = Strategies.getEvaluation(configuration.strategy.type);
		let points = strategy.evaluate(structure, configuration.strategy.configuration, undefined, undefined, feature);
		if (points) {
			document.dispatchEvent(new CustomEvent('scorePoints', { detail: { points: points, source: source } }));
		}
	}
}

Strategies.effects.scorePoints = new ScorePointsEffectStrategy();

/**
 * @typedef {Object} RemovePawnsEffectStrategyConfiguration
 * @property {RemovePawnsEffectStrategyConfigurationStrategy} strategy
 */

/**
 * @typedef {Object} RemovePawnsEffectStrategyConfigurationStrategy
 * @property {string} featureType
 * @property {ZoneData} zone
 * @property {EvaluationData} evaluation
 */

/**
 * Remove pawns strategy: give an extra turn to the current player.
 */
class RemovePawnsEffectStrategy extends EffectStrategy {
	constructor() {
		super();
		this.code = 'removePawns';
	}

	/**
	 * @param {Structure} structure
	 * @param {Feature} feature
	 * @param {Object} configuration
	 * @param {Player} currentPlayer
	 * @param {Pawn[]} newlyPlayedPawns
	 * @param {string} source
	 */
	apply(structure, feature, configuration, currentPlayer, newlyPlayedPawns, source) {
		Zones.getZone(configuration.zone, structure.initialX, structure.initialY).getNonEmptyBoxes().forEach((box) => {
			box.occupiedFeatures.forEach((occupiedFeature) => {
				if (configuration.featureType !== 'any' && configuration.featureType !== occupiedFeature.model.code) {
					return;
				}

				let occupiedStructure = new Structure(occupiedFeature, box);
				if (configuration.evaluation && configuration.evaluation.length) {
					let evaluator = new StructureEvaluator(occupiedStructure, occupiedFeature, currentPlayer, newlyPlayedPawns);
					evaluator.majors.forEach(function(player) {
						let points = evaluator.evaluate(player, configuration.evaluation);
						player.addPoints(points, '(' + occupiedFeature.name + ' [' + box.x + ', ' + box.y + ']) ' + source);
					});
				}

				occupiedStructure.removePawns();
			});
		});
	}
}

Strategies.effects.removePawns = new RemovePawnsEffectStrategy();