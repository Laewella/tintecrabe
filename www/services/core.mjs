/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
let Vue = window.Vue;

export let StringUtils = {
	/**
	 * @param {string} value
	 * @returns {string}
	 */
	ucfirst(value) {
		if (!value) {
			return '';
		}
		value = value.toString();
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
};

export let DateUtils = {
	timeFormat: new Intl.DateTimeFormat('fr-FR', { hour: 'numeric', minute: 'numeric', second: 'numeric' }),
	/**
	 * @param {Date} date
	 * @returns {string}
	 */
	time(date) {
		return this.timeFormat.format(date);
	}
};

export let MathUtils = {
	/**
	 * @param {number} min
	 * @param {number} max
	 * @returns {number}
	 */
	randomInt(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	}
};

export let ArrayUtils = {
	/**
	 * @param {Array} array
	 * @param {number} index
	 * @returns {number}
	 */
	nextIndex(array, index) {
		return (index + 1) < array.length ? (index + 1) : 0;
	},
	/**
	 * @param {Array} array
	 * @returns {number}
	 */
	randomIndex(array) {
		return MathUtils.randomInt(0, array.length);
	},
	/**
	 * @param {Array} array
	 * @returns {*}
	 */
	randomItem(array) {
		return array[MathUtils.randomInt(0, array.length)];
	},
	/**
	 * @param {Array} array1
	 * @param {Array} array2
	 * @returns {Array}
	 */
	merge(array1, array2) {
		for (let i = 0; i < array2.length; i++) {
			array1.push(array2[i]);
		}
		return array1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {boolean}
	 */
	contains(array, item) {
		return array.indexOf(item) > -1;
	},
	/**
	 * @param {Array} array
	 * @param {*} item
	 * @returns {Array}
	 */
	remove(array, item) {
		let index = array.indexOf(item);
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	clear(array) {
		array.splice(0, array.length);
		return array;
	},
	/**
	 * @param {Array} array
	 * @returns {Array}
	 */
	sortNumber(array) {
		array.sort(function(a, b) { return a - b; });
		return array;
	}
};

export let I18n = {
	LCID: 'fr_FR',
	keys: {},
	/**
	 * @param {string} LCID
	 * @param {function} endCallback
	 */
	init(LCID, endCallback) {
		this.LCID = LCID;
		let loader = new FileLoader();
		loader.loadFile('i18n/main/' + LCID + '.json', (data) => { this.importPackage(data, 'c.main.'); }, endCallback);
		loader.loadFile('i18n/setup/' + LCID + '.json', (data) => { this.importPackage(data, 'c.setup.'); }, endCallback);
	},
	/**
	 * @param {Object} data
	 * @param {string} prefix
	 */
	importPackage(data, prefix) {
		Object.keys(data).map((key) => {
			if (!('message' in data[key])) {
				console.warn('Ignore key declaration without message:', key);
				return;
			}
			let fullKey = prefix + key;
			if (fullKey in this.keys) {
				console.warn('Ignore key duplicate key declaration:', key);
				return;
			}
			this.keys[fullKey] = data[key].message;
		});
	},
	/**
	 * @param {string} key
	 * @param {string[]=} transformers
	 * @param {Object=} substitutions
	 * @returns {string}
	 */
	trans(key, transformers, substitutions) {
		let text = this.keys[key] || key;
		if (substitutions && substitutions.length) {
			substitutions.forEach(function(value, key) {
				text.replace('$' + key + '$', value);
			});
		}
		if (transformers) {
			transformers.forEach((transformer) => {
				if (transformer === 'etc') {
					text += '…';
				}
				else if (transformer === 'lab') {
					text += this.LCID === 'fr_FR' ? ' :' : ':';
				}
			});
		}
		return text;
	}
};

export let Timeline = {
	items: [],
	/**
	 * @param {string} message
	 */
	log(message) {
		this.items.unshift({
			message: message,
			date: new Date()
		});
	}
};

export let LocalStorage = {
	/**
	 * @param {string} key
	 * @returns {boolean}
	 */
	isset(key) {
		return this.get(key) !== null;
	},
	/**
	 * @param {string} key
	 * @param {string} value
	 */
	set(key, value) {
		localStorage.setItem('tc.' + key, value)
	},
	/**
	 * @param {string} key
	 * @param {Object} value
	 */
	setObject(key, value) {
		this.set(key, JSON.stringify(value));
	},
	/**
	 * @param {string} key
	 * @returns {string}
	 */
	get(key) {
		return localStorage.getItem('tc.' + key);
	},
	/**
	 * @param {string} key
	 * @returns {Object|null}
	 */
	getObject(key) {
		let value = this.get(key);
		return value ? JSON.parse(value) : null;
	}
};

export class FileLoader {
	constructor() {
		this.requests = 0;
	}

	/**
	 * @param {string} filePath
	 * @param {function} fileCallback
	 * @param {function} endCallback
	 */
	loadFile(filePath, fileCallback, endCallback) {
		this.requests++;
		fetch(filePath).then((response) => {
			if (response.ok) {
				response.json().then((data) => {
					fileCallback(data);
					this.requests--;
					if (!this.requests) {
						endCallback();
					}
				});
			}
			else {
				console.warn('filePath not found:', filePath);
				this.requests--;
				if (!this.requests) {
					endCallback();
				}
			}
		});
	}
}

//region Filters.

Vue.filter('ucfirst', (value) => {
	return StringUtils.ucfirst(value);
});

Vue.filter('time', (value) => {
	return DateUtils.time(value);
});

Vue.filter('trans', (value, transformers, substitutions) => {
	return I18n.trans(value, transformers, substitutions);
});

//endregion