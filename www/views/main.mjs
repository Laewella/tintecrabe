/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { I18n } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import setup from '../views/setup.mjs';
import play from '../views/play.mjs';
import extensions from '../views/extensions.mjs';

let Vue = window.Vue;
let VueRouter = window.VueRouter;

I18n.init('fr_FR', () => {
	Models.load(['core', 'mountains', 'deserts'], () => {
		let routes = [
			{ path: '/setup', component: setup },
			{ path: '/play', component: play },
			{ path: '/extensions', component: extensions }
		];

		let router = new VueRouter({
			routes: routes
		});

		let app = new Vue({
			router: router
		});

		app.$mount('#app');
	});
});