/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import '../components/all.mjs';
import { ArrayUtils, LocalStorage } from '../services/core.mjs';
import { Models } from '../services/models.mjs';
import { Player, Colors } from '../services/players.mjs';

/**
 * @typedef {Object} ExtensionData
 * @property {string} code
 * @property {Extension} extension
 * @property {TileData[]} tiles
 */

/**
 * @typedef {Object} TileData
 * @property {string} code
 * @property {TileModel} tile
 * @property {boolean} selected
 * @property {number} quantity
 */

export let Setup = {
	/** @type {boolean} */
	initialized: false,
	/** @type {Player[]} */
	players: [
		new Player('', null)
	],
	/** @type {Object[]} */
	pawns: [],
	/** @type {string} */
	poolType: 'selection',
	/** @type {number} */
	poolSize: 50,
	/** @type {ExtensionData[]} */
	tilesList: [],
	/** @type {TileModel[]} */
	startTiles: [],
	/** @type {TileModel|null} */
	startTile: null,
	/** @type {OptionalRules} */
	optionalRules: {},
	init() {
		if (this.initialized) {
			return;
		}
		this.initPawns();
		this.initTilesList();
		this.optionalRules.extraTurnWhenFillHole = true;
		this.initialized = true;
	},
	addPlayer() {
		this.players.push(new Player('', null));
	},
	/**
	 * @param {Player} player
	 */
	removePlayer(player) {
		ArrayUtils.remove(this.players, player)
	},
	initPawns() {
		Models.pawnsArray.forEach((pawnModel) => {
			let pawn = this.getPawn(pawnModel.code);
			if (!pawn) {
				this.pawns.push({ code: pawnModel.code, count: pawnModel.defaultCount });
			}
		});
	},
	/**
	 * @param {string} code
	 * @param {number} value
	 */
	setPawnCount(code, value) {
		let pawn = this.getPawn(code);
		if (pawn) {
			pawn.count = value;
		}
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getPawn(code) {
		let result = null;
		this.pawns.forEach((pawn) => {
			if (pawn.code === code) {
				result = pawn;
			}
		});
		return result;
	},
	initTilesList() {
		Models.extensionsArray.forEach((extension) => {
			if (!extension.tiles.length) {
				return;
			}

			let item = {
				code: extension.code,
				extension: extension,
				tiles: []
			};

			extension.tiles.forEach((tileModel) => {
				item.tiles.push({
					code: tileModel.code,
					tile: tileModel,
					selected: true,
					quantity: tileModel.defaultQuantity
				});

				if (tileModel.start) {
					this.startTiles.push(tileModel);
				}
			});

			this.tilesList.push(item);
		});
		this.startTile = this.startTiles[0];
	},
	selectAllTiles() {
		this.tilesList.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				tileItem.selected = true;
			});
		});
	},
	deselectAllTiles() {
		this.tilesList.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				tileItem.selected = false;
			});
		});
	},
	/**
	 * @param {Object} extension
	 */
	selectExtensionTiles(extension) {
		extension.tiles.forEach((tileItem) => {
			tileItem.selected = true;
		});
	},
	/**
	 * @param {Object} extension
	 */
	deselectExtensionTiles(extension) {
		extension.tiles.forEach((tileItem) => {
			tileItem.selected = false;
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	selectTilesWithFeature(featureModel) {
		this.tilesList.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				if (tileItem.tile.containsFeature(featureModel.code)) {
					tileItem.selected = true;
				}
			});
		});
	},
	/**
	 * @param {FeatureModel} featureModel
	 */
	deselectTilesWithFeature(featureModel) {
		this.tilesList.forEach((extension) => {
			extension.tiles.forEach((tileItem) => {
				if (tileItem.tile.containsFeature(featureModel.code)) {
					tileItem.selected = false;
				}
			});
		});
	},
	/**
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getExtension(code) {
		let result = null;
		this.tilesList.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @param {Object} extension
	 * @param {string} code
	 * @returns {Object|null}
	 */
	getTileData(extension, code) {
		let result = null;
		extension.tiles.forEach((item) => {
			if (item.code === code) {
				result = item;
			}
		});
		return result;
	},
	/**
	 * @returns {TileModel[]}
	 */
	get selectedTileModels() {
		let selectedTileModels = [];
		this.tilesList.forEach((item) => {
			item.tiles.forEach((tileItem) => {
				if (tileItem.selected) {
					for (let i = 0; i < tileItem.quantity; i++) {
						selectedTileModels.push(tileItem.tile);
					}
				}
			});
		});
		return selectedTileModels;
	},
	/**
	 * @returns {boolean}
	 */
	get isValid() {
		let isValid = false;
		Object.keys(this.pawns).forEach((code) => {
			if (this.pawns[code]) {
				isValid = true;
			}
		});
		this.players.forEach((player) => {
			if (!player.name || !player.color) {
				isValid = false;
			}
		});
		return isValid && this.poolSize > 1 && this.selectedTileModels.length;
	},
	/**
	 * @returns {Object}
	 */
	toData() {
		let data = {
			poolSize: this.poolSize,
			poolType: this.poolType,
			startTile: this.startTile.code,
			players: [],
			pawns: [],
			tilesList: []
		};

		this.players.forEach((player) => {
			data.players.push({
				name: player.name,
				color: player.color.code
			});
		});

		this.pawns.forEach((pawn) => {
			data.pawns.push({
				code: pawn.code,
				count: pawn.count
			});
		});

		this.tilesList.forEach((item) => {
			let itemData = {
				code: item.code,
				tiles: []
			};

			item.tiles.forEach((tileItem) => {
				itemData.tiles.push({
					code: tileItem.code,
					selected: tileItem.selected,
					quantity: tileItem.quantity
				});
			});

			data.tilesList.push(itemData);
		});

		data.optionalRules = {
			'extraTurnWhenFillHole': this.optionalRules.extraTurnWhenFillHole
		};

		return data;
	},
	/**
	 * @param {Object|null} data
	 */
	fromData(data) {
		if (!data) {
			return;
		}

		if (data.hasOwnProperty('poolSize')) {
			this.poolSize = data.poolSize;
		}

		if (data.hasOwnProperty('poolType')) {
			this.poolType = data.poolType;
		}

		if (data.hasOwnProperty('startTile')) {
			Models.extensionsArray.forEach((extension) => {
				extension.tiles.forEach((tileModel) => {
					if (data.startTile === tileModel.code) {
						this.startTile = tileModel;
					}
				});
			});
		}

		if (data.hasOwnProperty('players')) {
			ArrayUtils.clear(Setup.players);
			data.players.forEach((item) => {
				this.players.push(new Player(item.name, Colors[item.color]));
			});
		}

		if (data.hasOwnProperty('pawns')) {
			data.pawns.forEach((item) => {
				this.setPawnCount(item.code, item.count);
			});
		}

		if (data.hasOwnProperty('tilesList')) {
			data.tilesList.forEach((extensionItem) => {
				let extensionData = this.getExtension(extensionItem.code);
				if (extensionData) {
					extensionItem.tiles.forEach((tileItem) => {
						let tileData = this.getTileData(extensionData, tileItem.code);
						if (tileData) {
							tileData.selected = tileItem.selected;
							if (tileItem.selected && tileItem.hasOwnProperty('quantity')) {
								tileData.quantity = tileItem.quantity;
							}
						}
					});
				}
			});
		}

		if (data.optionalRules) {
			if (data.optionalRules.hasOwnProperty('extraTurnWhenFillHole')) {
				this.optionalRules.extraTurnWhenFillHole = data.optionalRules.extraTurnWhenFillHole;
			}
		}
	},
	/**
	 * @returns {boolean}
	 */
	hasLast() {
		return LocalStorage.isset('lastGameSetup');
	},
	saveLast() {
		LocalStorage.setObject('lastGameSetup', this.toData());
	},
	loadLast() {
		this.fromData(LocalStorage.getObject('lastGameSetup'));
	}
};