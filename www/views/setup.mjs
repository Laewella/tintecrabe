/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Setup } from '../services/setup.mjs';
import { Game } from '../services/game.mjs';

export default {
	data: function() {
		Setup.init();
		return {
			Setup: Setup,
			start() {
				Game.init();
				this.$router.push('/play');
			}
		}
	},
	template: `<div id="view-setup">
			<div>
				<h1>{{ 'c.setup.game_configuration' | trans }}</h1>
				<div v-if="Setup.hasLast">
					<button @click="Setup.loadLast()">{{ 'c.setup.load_last_configuration' | trans }}</button>
				</div>
				<tc-players-setup></tc-players-setup>
				<tc-pawns-setup></tc-pawns-setup>
				<tc-pool-setup></tc-pool-setup>
				<tc-optional-rules-setup></tc-optional-rules-setup>
				<div>
					<button :disabled="!Setup.isValid" @click="start()">{{ 'c.setup.start_game' | trans }}</button>
				</div>
			</div>
			<div>
				<tc-first-tile-setup></tc-first-tile-setup>
				<tc-tiles-list-setup></tc-tiles-list-setup>
			</div>
		</div>`
}