/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
(function(window, document) {
	'use strict';

	let peer = new Peer();
	let peerId = null;

	peer.on('open', function(id) {
		peerId = id;
		console.log('My peer ID is:', peerId);
		document.getElementById('step-wait').classList.add('hidden');
		document.getElementById('step-start').classList.remove('hidden');
	});

	let Connection = {};

	let Server = {
		connections: [],
		start: function() {
			document.getElementById('server-id').appendChild(document.createTextNode(peerId));
			document.getElementById('server-step-one').classList.add('hidden');
			document.getElementById('server-step-two').classList.remove('hidden');

			peer.on('connection', function(connection) {
				Server.connections.push(connection);
				console.log('[Server.start]', 'Connection:', connection);

				connection.on('data', function(data) {
					console.log('[Server]', 'Received', data);
				});
			});
		}
	};

	let Client = {
		connection: null,
		connect: function() {
			let serverId = document.getElementById('server-id-input').value;
			Client.connection = peer.connect(serverId);
			console.log('[Client.connect]', 'Connected to:', serverId);

			Client.connection.on('data', function(data) {
				console.log('[Client]', 'Received', data);
			});
		},
		send: function() {
		console.log('[Client]', 'Send', document.getElementById('client-message').value);
			Client.connection.send(document.getElementById('client-message').value);
		}
	};

	if (!window.Tintecrabe) {
		window.Tintecrabe = {};
	}

	window.Tintecrabe.Server = Server;
	window.Tintecrabe.Client = Client;
})(window, window.document);