/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import { Zones } from '../services/board.mjs';

let Vue = window.Vue;

let vm = new Vue({
	el: '#zones-view',
	data: {
		items: [-3, -2, -1, 0, 1, 2, 3],
		zones: [
			{
				label: 'square {x = 0, y = 0, length = 0}',
				zone: Zones.getSquare(0, 0, 0)
			},
			{
				label: 'square {x = 0, y = 0, length = 1}',
				zone: Zones.getSquare(0, 0, 1)
			},
			{
				label: 'square {x = 0, y = 0, length = 2}',
				zone: Zones.getSquare(0, 0, 2)
			},
			{
				label: 'square {x = -1, y = 1, length = 1}',
				zone: Zones.getSquare(-1, 1, 1)
			},
			{
				label: 'diamond {x = 0, y = 0, length = 0}',
				zone: Zones.getDiamond(0, 0, 0)
			},
			{
				label: 'diamond {x = 0, y = 0, length = 1}',
				zone: Zones.getDiamond(0, 0, 1)
			},
			{
				label: 'diamond {x = 0, y = 0, length = 2}',
				zone: Zones.getDiamond(0, 0, 2)
			},
			{
				label: 'diamond {x = -1, y = 1, length = 1}',
				zone: Zones.getDiamond(-1, 1, 1)
			},
			{
				label: 'cross {x = 0, y = 0, length = 0}',
				zone: Zones.getCross(0, 0, 0)
			},
			{
				label: 'cross {x = 0, y = 0, length = 1}',
				zone: Zones.getCross(0, 0, 1)
			},
			{
				label: 'cross {x = 0, y = 0, length = 2}',
				zone: Zones.getCross(0, 0, 2)
			},
			{
				label: 'cross {x = -1, y = 1, length = 1}',
				zone: Zones.getCross(-1, 1, 1)
			},
			{
				label: 'ix {x = 0, y = 0, length = 0}',
				zone: Zones.getIx(0, 0, 0)
			},
			{
				label: 'ix {x = 0, y = 0, length = 1}',
				zone: Zones.getIx(0, 0, 1)
			},
			{
				label: 'ix {x = 0, y = 0, length = 2}',
				zone: Zones.getIx(0, 0, 2)
			},
			{
				label: 'ix {x = -1, y = 1, length = 1}',
				zone: Zones.getIx(-1, 1, 1)
			},
		]
	}
});