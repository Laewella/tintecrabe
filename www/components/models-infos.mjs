/**
 * Copyright (C) 2020 Darathor
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
import {ArrayUtils} from '../services/core.mjs';
import {Models} from '../services/models.mjs';

let Vue = window.Vue;

Vue.component('models-infos-modals', {
	data: function() {
		return {
			Details: Models.Details
		}
	},
	template: `<modal title="Détail de la tuile" modal-style="width: 610px;" v-if="Details.tile" @close="Details.tile = null">
			<div slot="body">
				<tile-description :tile="Details.tile"></tile-description>
			</div>
		</modal>`
});

Vue.component('tile-description', {
	props: ['tile'],
	data: function() {
		return {
			feature: null
		}
	},
	methods: {
		selectFeature(feature) {
			this.feature = feature;
		},
		isNeighbour(index) {
			return this.feature && ArrayUtils.contains(this.feature[3], index);
		}
	},
	template: `<div class="tile-description flex-horizontal">
			<div class="tile-container" v-if="tile">
				<img :src="tile.src" alt="" />
				<span v-for="(item, index) in tile.featuresData" :style="{left: item[1] + '%', top: item[2] + '%' }"
					:class="['feature', {'current': item === feature, 'neighbour': isNeighbour(index)}]"
					title="Cliquer pour voir les informations détaillées de l'élément" @click="selectFeature(item)"></span>
			</div>
			<div class="feature-container" v-if="feature">
				<h4>Informations sur l'élément sélectionné</h4>
				<tc-feature-model-summary :code="feature[0]"></tc-feature-model-summary>
			</div>
		</div>`
});

Vue.component('tc-feature-model-summary', {
	props: ['code', 'model', 'feature'],
	data: function() {
		return {
			item: this.model || (this.feature ? this.feature.model : Models.features[this.code])
		};
	},
	watch: {
		code: function() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		model: function() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		},
		feature: function() {
			this.item = this.model || (this.feature ? this.feature.model : Models.features[this.code]);
		}
	},
	template: `<div class="tc-feature-model-summary">
			<img v-if="item.src" :src="item.src" alt="" class="thumbnail" />
			{{ item.name | ucfirst }} <small class="text-muted">{{ item.code }}</small>
		</div>`
});

Vue.component('tc-pawn-model-summary', {
	props: ['code', 'model', 'pawn'],
	data: function() {
		return {
			item: this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code])
		};
	},
	watch: {
		code: function() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		},
		model: function() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		},
		pawn: function() {
			this.item = this.model || (this.pawn ? this.pawn.model : Models.pawns[this.code]);
		}
	},
	template: `<span class="tc-pawn-model-summary">
			<span class="pawn pawn-26"><img :src="item.defaultSrc" alt="" /></span>
			{{ item.name | ucfirst }} <small class="text-muted">{{ item.code }}</small>
		</span>`
});